# Introduction

It is a simple Android currency converter written in Kotlin using GitLab CI / CD and [fastlane](https://fastlane.tools/).

# Terminology

**Konverter** - project name (not to be confused with converter)

**Converter** - a feature of the project that converts money amounts into other currencies

**RateSet** - a set of Rate pairs to one base currency

**Rate** - a ratio of 2 divisible to dividend currencies

**ru.int64** - domain of an imaginary company which uses the following simple package naming approach:
 - **ru.int64.[projectname]** - for projects: apps and reusable high-level libraries
 - **ru.int64.lib.[libname]** - for auxilliary and low-level libs for internal usage shared between projects
 
# Architecture

Multi-module implementation with multiple abilities for extending and reusage of separate modules.
Package-by-feature and package-by-layer are mixed which might look like overengineering for such a project. Therefore there is a short explanation regarding the modules splitting below.

# Technologies

## App

**MVICore** is used for business logic. An MVI implementation by Badoo (com.github.badoo.mvicore)

**Koin** for DI

**Retrofit** for Rest API interaction

**RxJava2** for Rx (Rx3 is not still supported by MVICore)

## Tests

**Spek Framework** - JetBrains' specification-style test framework

**Jacoco** for code coverage

**AssertJ** for assertions

**Mockk** for mocks


# Modules and packages

## Simplified modules diagram

![Simplified modules diagram](modules-dia.png)

### With shared libraries

![Simplified modules diagram with libraries](modules-dia-with-libs.png)

```
:app

:rates:interactor-rates
:rates:module-rates-data
:rates:module-rates-amount-input-persistence

:features:ui-converter
:features:ui-viewer
:features:core-converter

:features-shared:ui-ratefeatures
:features-shared:res-flags

:libs-rateset:rateset-repo
:libs-rateset:rateset-ds-api
:libs-rateset:rateset-ds-persist
:libs-rateset:rateset-domain

:libs-persist-input:persist-input-repo
:libs-persist-input:persist-input-ds

:libs-int64:lib-int64-appresult
:libs-int64:lib-int64-network
:libs-int64:lib-int64-mvicore-lifecycle
:libs-int64:lib-int64-mvicore-lifecycle-android
```



All the modules can be divided into:

- **App** itself

- **App modules**
	- DI 'modules' (modules in terms of Koin)
	
- **Features**
    - UI modules imported into the app (Fragments)
    - Core modules - features in terms of MVICore (see https://badoo.github.io/MVICore/features/coreconcepts/)

- **Libraries** - used by the the previous modules or other libraries

## App modules

### :konverter-*

Modules in the ':app-modules' folder are highly specific for the project and are not supposed to be reused anywhere else.
Each of them except have a single DI module which are used in the :app

**:app** (ru.int64.konverter.app) - application

**:konverter-rates-data** (ru.int64.konverter.data) - bundles all data related libs into a DI module

**:konverter-rates-interactor** (ru.int64.konverter.interactor) - classically interactor, includes Use Cases and Repo interfaces used by them

These modules are specific for the 'currency rates' domain. They can enlarge if more rates related features will be added or new
data/interactor can be introduced if some conceptually new functionality will be added 

### :features:core-*

'Features' in the MVI terminology.

**(sic!)** *Functionality of the rate Viewer is a subset of the Converter feature functionality. Therefore rate Viewer uses the same 'core' feature.*

Includes:
- MVI feature implementation: Actor, Reducer, Bootstrapper etc
- DI module

**:core-converter** (ru.int64.konverter.converter) - feature that can load RateSets for given base currency and convert sums from one currency into another

The features are supposed to be reused entirely as solid features but unleashed from UI.

**:feature:core-** module can be reused with an alternative **:feature:ui-** implementation

### :features:ui-*

'Fragment' modules that contain single or multiple coupled Fragments.
The module itself is coupled with a related **features:core-** module

**:features:ui-converter** (ru.int64.konverter.converter)

**:feature:ui-viewer** (ru.int64.konverter.viewer)

*For the same of simplicity both **ui-** modules are coupled with the same **-core** module. They would have been split into different -core modules
if Viewer have had any unique functionality that Converter doesn't contain*

The **ui-** modules are only supposed to be reused together with respective **core-** module as a solid Fragment as is. 

### :features-shared:ui-ratefeatures*

Does not represent a specific module but helps to better organize the features-related code which can be shared between multiple features.

In case of reusage of the features modules the 'shared' module should be brought together with the features or eliminated
by moving shared code into features or another libs.

The only reason of this module is to avoid further reusage-oriented overengineering

## Libraries

### :libs-rateset:*

Contain everything related the RateSet from domain model to implementation of the Revolut hiring.revolut.codes API.
They can be reused together in another app/features operating with Currencies, they Rates and receiving so-called RateSets
from the provided or similar APIs

**:lib-rateset-domain** - all entities: Currency, Amount, Rate, RateSet + extensions and syntax sugar, mostly for better tests code

**:lib-rateset-repo** - implementation of the Repo interface from the :konverter-interactor module and RateDs interface declaration

**:lib-rateset-ds** - implementation of the above mentioned interface for the Revolut API

### :libs-int64:*

These are the libraries of the imaginary company with int64.ru domain than can potentially be shared within the company

**:lib-int64-appresult** - contains single simple AppResult class

**:lib-int64-network** - contains some auxilary classes for network API implementation

**:lib-int64-network-statechecker** - a ds for checking network state of the device

**:lib-int64-mvicore-lifecycle** - extends functionality of MVICore introducing core-level lifecycle along with the lifecycle of view updates
Since features are 'hot' observables they require some additional management for suspension. This library implements it in a generic way

**:lib-int64-mvicore-lifecycle-android** - implementation of the concept introduced in the prev module for Android
 
# How to run tests

Since tests are written with Spek, to run them from Android Studio **Spek Framework plugin is required**

They can also bee run with gradle 'test' task but the output might be not informative in that case.
Spek provides tree view spek-style outputs while gradle prints a plain list of the leaves of the tree.

Tests with coverage are also run in the GitLab CI. To see the results:
 
1. Open CI / CD -> Pipelines
2. Find the last run task sequence
3. Click the 3rd tick icon ('test: passed') -> 'test debug'
4. On the job page 'Job artifacts' -> 'Browse'

## What's contained in this project

### Fastlane files

It also has fastlane setup per our [blog post](https://about.gitlab.com/2019/01/28/android-publishing-with-gitlab-and-fastlane/) on
getting GitLab CI set up with fastlane. Note that you may want to update your
fastlane bundle to the latest version; if a newer version is available, the pipeline
job output will tell you.

### Dockerfile build environment

In the root there is a Dockerfile which defines a build environment which will be
used to ensure consistent and reliable builds of your Android application using
the correct Android SDK and other details you expect. Feel free to add any
build-time tools or whatever else you need here.

We generate this environment as needed because installing the Android SDK
for every pipeline run would be very slow.

### Build configuration (`.gitlab-ci.yml`)

The sample project also contains a basic `.gitlab-ci.yml` which will successfully 
build the Android application.

Note that for publishing to the test channels or production, you'll need to set
up your secret API key. The stub code is here for that, but please see our
[blog post](https://about.gitlab.com/2019/01/28/android-publishing-with-gitlab-and-fastlane/) for
details on how to set this up completely. In the meantime, publishing steps will fail.

The build script also handles automatic versioning by relying on the CI pipeline
ID to generate a unique, ever increasing number. If you have a different versioning
scheme you may want to change this.

```yaml
    - "export VERSION_CODE=$(($CI_PIPELINE_IID)) && echo $VERSION_CODE"
    - "export VERSION_SHA=`echo ${CI_COMMIT_SHA:0:8}` && echo $VERSION_SHA"
```