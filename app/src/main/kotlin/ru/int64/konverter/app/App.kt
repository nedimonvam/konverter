package ru.int64.konverter.app

import android.app.Application
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.plugins.RxJavaPlugins
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.qualifier.named
import org.koin.dsl.module
import ru.int64.konverter.converter.feature.ConverterFeature
import ru.int64.konverter.converter.fragment.ConverterFeatureFragment
import ru.int64.konverter.data.moduleRatesData
import ru.int64.konverter.persistinput.modulePersistAmountInputFor
import ru.int64.konverter.viewer.fragment.ViewerFeatureFragment
import ru.int64.rateset.ext.amountOf


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(
                moduleRatesData(
                    apiUrl = BuildConfig.API_URL,
                    isLoggingEnabled = BuildConfig.DEBUG
                ),
                module {
                    single(named<ViewerFeatureFragment>()) {
                        ConverterFeature(
                            rateRepo = get(),
                            amountInputRepo = get(named<ViewerFeatureFragment>()),
                            mainThreadScheduler = AndroidSchedulers.mainThread(),
                            defaultAmount = 1 amountOf BuildConfig.DEFAULT_CURRENCY,
                            updatePeriodInSeconds = BuildConfig.UPDATE_PERIOD
                        )
                    }
                },
                modulePersistAmountInputFor<ViewerFeatureFragment>(),
                module {
                    single(named<ConverterFeatureFragment>()) {
                        ConverterFeature(
                            rateRepo = get(),
                            amountInputRepo = get(named<ConverterFeatureFragment>()),
                            mainThreadScheduler = AndroidSchedulers.mainThread(),
                            defaultAmount = 1 amountOf BuildConfig.DEFAULT_CURRENCY,
                            updatePeriodInSeconds = BuildConfig.UPDATE_PERIOD
                        )
                    }
                },
                modulePersistAmountInputFor<ConverterFeatureFragment>()
            )
        }
        RxJavaPlugins.setErrorHandler {
            Log.e("RxJavaPlugins", it.message, it)
        }
    }
}