package ru.int64.konverter.app.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import ru.int64.konverter.app.databinding.FragmentViewpagerBinding
import ru.int64.lib.mvilifecycle.android.viewpager.LifecycleFragmentOnPageChangeListener


class ViewPagerFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentViewpagerBinding
        .inflate(inflater, container, false)
        .also { setupFragment(it) }
        .root

    private fun setupFragment(binding: FragmentViewpagerBinding) {
        val viewPagerAdapter = ViewPagerAdapter(parentFragmentManager, requireContext())
        binding.viewpager.apply {
            adapter = viewPagerAdapter
            binding.tabLayout.setupWithViewPager(this)
            addOnPageChangeListener(
                LifecycleFragmentOnPageChangeListener(
                    viewPagerAdapter
                )
                    .also { post { it.onPageSelected(currentItem) } })
        }
    }
}