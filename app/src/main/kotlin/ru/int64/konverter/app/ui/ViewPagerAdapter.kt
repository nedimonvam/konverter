package ru.int64.konverter.app.ui

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import ru.int64.konverter.app.R
import ru.int64.konverter.converter.fragment.ConverterFeatureFragment
import ru.int64.konverter.viewer.fragment.ViewerFeatureFragment

class ViewPagerAdapter(
    fragmentManager: FragmentManager,
    private val context: Context
) : FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val pages: List<Pair<Fragment, Int>> = listOf(
        ViewerFeatureFragment() to R.string.pagertitle_viewer,
        ConverterFeatureFragment() to R.string.pagertitle_converter
    )

    override fun getItem(position: Int) = pages[position].first

    override fun getPageTitle(position: Int): String = context.getString(pages[position].second)

    override fun getCount() = pages.size
}