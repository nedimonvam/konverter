package ru.int64.lib.lib.mvi.lifecycle

import com.badoo.mvicore.binder.lifecycle.Lifecycle
import io.reactivex.functions.Consumer

interface LifecycleFeature {
    val lifecycleConsumer: Consumer<Lifecycle.Event>
}