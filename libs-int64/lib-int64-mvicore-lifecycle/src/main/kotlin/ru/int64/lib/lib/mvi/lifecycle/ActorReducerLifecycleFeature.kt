package ru.int64.lib.lib.mvi.lifecycle

import com.badoo.mvicore.binder.lifecycle.Lifecycle
import com.badoo.mvicore.element.Actor
import com.badoo.mvicore.element.Bootstrapper
import com.badoo.mvicore.element.NewsPublisher
import com.badoo.mvicore.element.Reducer
import com.badoo.mvicore.feature.ActorReducerFeature
import io.reactivex.functions.Consumer
import io.reactivex.subjects.BehaviorSubject

open class ActorReducerLifecycleFeature<Wish : Any, in Effect : Any, State : Any, News : Any>(
    initialState: State,
    bootstrapper: Bootstrapper<Wish>? = null,
    actor: Actor<State, Wish, Effect>,
    reducer: Reducer<State, Effect>,
    newsPublisher: NewsPublisher<Wish, Effect, State, News>? = null,
    coreLifecycleEvents: BehaviorSubject<Lifecycle.Event> = BehaviorSubject.create()
) : LifecycleFeature, ActorReducerFeature<Wish, Effect, State, News>(
    initialState,
    bootstrapper,
    actor,
    reducer,
    newsPublisher
) {

    override val lifecycleConsumer = Consumer<Lifecycle.Event> {
        coreLifecycleEvents.onNext(it)
    }
}