package ru.int64.lib.lib.mvi.lifecycle.ext

import com.badoo.mvicore.binder.lifecycle.Lifecycle.Event
import com.badoo.mvicore.binder.lifecycle.Lifecycle.Event.BEGIN
import com.badoo.mvicore.binder.lifecycle.Lifecycle.Event.END
import io.reactivex.Observable
import io.reactivex.Observable.combineLatest
import io.reactivex.ObservableSource
import io.reactivex.functions.BiFunction

fun foldLifecycleSources(
    events: List<ObservableSource<Event>>
) = events.fold(Observable.just(BEGIN)) { folded, next ->
    combineLatest(folded, next, BiFunction<Event, Event, Event> { event1, event2 ->
        when {
            event1 == BEGIN && event2 == BEGIN -> BEGIN
            else -> END
        }
    })
}