package ru.int64.lib.appresult.ext

import ru.int64.lib.appresult.AppResult

val <T> AppResult<T>.valueOrNull
    get() = (this as? AppResult.Success)?.value

val <T> AppResult<T>.errorOrNull
    get() = (this as? AppResult.Error)

fun <T> AppResult<T>.onError(function: (AppResult.Error) -> Any?) = apply {
    errorOrNull?.let { function(it) }
}

fun <T> AppResult<T>.onSuccess(function: (T) -> Any?) = apply {
    valueOrNull?.let { function(it) }
}

fun <T, R> AppResult<T>.map(transform: (T) -> R): AppResult<R> {
    return when (this) {
        is AppResult.Success -> AppResult.Success(transform(value))
        is AppResult.Error -> this
    }
}

fun <T, R> AppResult<T>.flatMap(transform: (T) -> AppResult<R>): AppResult<R> {
    return when (this) {
        is AppResult.Success -> transform(value)
        is AppResult.Error -> this
    }
}
