package ru.int64.lib.appresult

sealed class AppResult<out T> {

    data class Success<out T>(val value: T) : AppResult<T>()

    data class Error(val throwable: Throwable) : AppResult<Nothing>()
}
