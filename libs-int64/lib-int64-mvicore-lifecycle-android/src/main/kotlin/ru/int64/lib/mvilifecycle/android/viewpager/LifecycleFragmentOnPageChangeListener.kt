package ru.int64.lib.mvilifecycle.android.viewpager

import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager

class LifecycleFragmentOnPageChangeListener(
    private val adapter: FragmentPagerAdapter
) : ViewPager.OnPageChangeListener {

    private var currentPosition: Int? = null
    private val currentFragment
        get() = adapter.getLifecycleItem(currentPosition)

    override fun onPageSelected(position: Int) {
        currentFragment?.onPauseFragment()
        adapter.getLifecycleItem(position)?.onResumeFragment()
        currentPosition = position
    }

    override fun onPageScrollStateChanged(state: Int) {
        currentFragment?.apply {
            when (state) {
                ViewPager.SCROLL_STATE_IDLE -> onResumeFragment()
                else -> onPauseFragment()
            }
        }
    }

    override fun onPageScrolled(
        position: Int,
        positionOffset: Float,
        positionOffsetPixels: Int
    ) = Unit

    companion object {
        fun FragmentPagerAdapter.getLifecycleItem(
            position: Int?
        ) = position?.let { getItem(it) } as? ViewPagerFragmentLifecycle
    }
}