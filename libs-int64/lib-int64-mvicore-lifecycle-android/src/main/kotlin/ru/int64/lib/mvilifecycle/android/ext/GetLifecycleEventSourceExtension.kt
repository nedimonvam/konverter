package ru.int64.lib.mvilifecycle.android.ext

import androidx.lifecycle.LifecycleOwner
import com.badoo.mvicore.android.lifecycle.BaseAndroidBinderLifecycle
import com.badoo.mvicore.binder.lifecycle.Lifecycle
import io.reactivex.ObservableSource
import ru.int64.lib.mvilifecycle.android.bindings.BinderBehavior
import ru.int64.lib.mvilifecycle.android.GenericLifecycleObserver

fun LifecycleOwner.getLifecycleEventSource(
    behavior: BinderBehavior
): ObservableSource<Lifecycle.Event> = object : BaseAndroidBinderLifecycle(
    lifecycle,
    { sendEvent -> GenericLifecycleObserver(sendEvent, behavior) }
) {}