package ru.int64.lib.mvilifecycle.android.viewpager

import com.badoo.mvicore.binder.lifecycle.Lifecycle.Event
import com.badoo.mvicore.binder.lifecycle.Lifecycle.Event.BEGIN
import com.badoo.mvicore.binder.lifecycle.Lifecycle.Event.END
import io.reactivex.Observer

interface ViewPagerFragmentLifecycle {

    val viewPagerLifecycleEvents: Observer<Event>

    fun onResumeFragment() = viewPagerLifecycleEvents.onNext(BEGIN)

    fun onPauseFragment() = viewPagerLifecycleEvents.onNext(END)
}