package ru.int64.lib.mvilifecycle.android.bindings

import androidx.lifecycle.LifecycleObserver
import com.badoo.mvicore.binder.lifecycle.Lifecycle
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.subjects.BehaviorSubject


class BinderLifecycle private constructor(
    lifecycleEvents: Observable<Lifecycle.Event>,
    subject: BehaviorSubject<Lifecycle.Event>
) : Lifecycle,
    ObservableSource<Lifecycle.Event> by subject,
    LifecycleObserver {

    init {
        lifecycleEvents.subscribe(subject)
    }

    constructor(
        lifecycleEvents: Observable<Lifecycle.Event>
    ) : this(lifecycleEvents, BehaviorSubject.create())
}

enum class BinderBehavior {
    ResumePause,
    CreateDestroy,
    StartStop
}

