package ru.int64.lib.mvilifecycle.android

import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.badoo.mvicore.binder.lifecycle.Lifecycle
import ru.int64.lib.mvilifecycle.android.bindings.BinderBehavior

class GenericLifecycleObserver(
    private val sendEvent: ((Lifecycle.Event) -> Unit),
    private val binderBehavior: BinderBehavior
) : DefaultLifecycleObserver {

    override fun onCreate(owner: LifecycleOwner) {
        if (binderBehavior == BinderBehavior.CreateDestroy) begin()
    }

    override fun onStart(owner: LifecycleOwner) {
        if (binderBehavior == BinderBehavior.StartStop) begin()
    }

    override fun onResume(owner: LifecycleOwner) {
        if (binderBehavior == BinderBehavior.ResumePause) begin()
    }

    override fun onPause(owner: LifecycleOwner) {
        if (binderBehavior == BinderBehavior.ResumePause) end()
    }

    override fun onStop(owner: LifecycleOwner) {
        if (binderBehavior == BinderBehavior.StartStop) end()
    }

    override fun onDestroy(owner: LifecycleOwner) {
        if (binderBehavior == BinderBehavior.CreateDestroy) end()
    }

    private fun begin() = sendEvent(Lifecycle.Event.BEGIN)

    private fun end() = sendEvent(Lifecycle.Event.END)
}