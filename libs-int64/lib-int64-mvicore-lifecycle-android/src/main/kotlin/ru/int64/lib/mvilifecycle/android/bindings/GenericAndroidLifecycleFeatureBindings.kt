package ru.int64.lib.mvilifecycle.android.bindings

import android.annotation.SuppressLint
import com.badoo.mvicore.binder.Binder
import com.badoo.mvicore.binder.lifecycle.Lifecycle
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import ru.int64.lib.lib.mvi.lifecycle.LifecycleFeature

@SuppressLint("CheckResult")
abstract class GenericAndroidLifecycleFeatureBindings<TUiEvent, TViewModel, TNews>(
    lifecycleFeature: LifecycleFeature,
    viewLifecycleEvents: Observable<Lifecycle.Event>,
    coreLifecycleEvents: Observable<Lifecycle.Event>,
    setup: (Binder) -> Unit
) {
    private val viewBinder = Binder(BinderLifecycle(viewLifecycleEvents))

    init {
        coreLifecycleEvents.subscribe(lifecycleFeature.lifecycleConsumer)
        setup(viewBinder)
    }


}
