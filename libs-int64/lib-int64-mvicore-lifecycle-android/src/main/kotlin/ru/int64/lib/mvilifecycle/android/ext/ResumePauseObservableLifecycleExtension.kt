package ru.int64.lib.mvilifecycle.android.ext

import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import io.reactivex.Observable
import io.reactivex.Observer
import com.badoo.mvicore.binder.lifecycle.Lifecycle
import io.reactivex.subjects.PublishSubject

fun LifecycleOwner.resumePauseObservableLifecycle() = object : Observable<Lifecycle.Event>() {

    private val lifecycleEvents = PublishSubject.create<Lifecycle.Event>()

    val observer = object : DefaultLifecycleObserver {
        override fun onResume(owner: LifecycleOwner) {
            lifecycleEvents.onNext(Lifecycle.Event.BEGIN)
        }

        override fun onPause(owner: LifecycleOwner) {
            lifecycleEvents.onNext(Lifecycle.Event.END)
        }
    }

    init {
        lifecycle.addObserver(observer)
    }

    override fun subscribeActual(observer: Observer<in Lifecycle.Event>?) {
        observer?.let { lifecycleEvents.subscribe(it) }
    }

}