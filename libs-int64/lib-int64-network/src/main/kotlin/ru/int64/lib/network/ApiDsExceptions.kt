package ru.int64.lib.network

import java.lang.RuntimeException

class NetworkException(
    cause: Throwable?,
    message: String? = cause?.message
) : RuntimeException(message, cause)

class MappingException(
    cause: Throwable?,
    message: String? = cause?.message
) : RuntimeException(message, cause) {

    constructor(message: String?) : this(null, message)
}

class HttpResponseCodeException(code: Int) : RuntimeException(
    "Endpoint returned HTTP response with code $code"
)