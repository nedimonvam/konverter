package ru.int64.lib.network.networkstate

interface NetworkConnectionDs {

    fun getIsConnected(): Boolean
}