package ru.int64.lib.network

import retrofit2.Call
import ru.int64.lib.appresult.AppResult
import ru.int64.lib.appresult.ext.flatMap
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException

abstract class BaseApiDs {

    fun <T, R> runApiCall(
        call: Call<T>,
        mappingFunction: (T) -> R
    ) = try {
        val response = call.execute()
        when (val code = response.code()) {
            HTTP_CODE_SUCCESS -> when (val body = response.body()) {
                null -> AppResult.Error(NetworkException(null, "No response"))
                else -> AppResult.Success(body)
            }
            else -> AppResult.Error(HttpResponseCodeException(code))
        }
    } catch (e: Exception) {
        AppResult.Error(
            when (e) {
                is ConnectException,
                is UnknownHostException,
                is TimeoutException,
                is SocketTimeoutException -> NetworkException(e, "No connection")
                is IllegalArgumentException,
                is IllegalStateException -> MappingException(e)
                else -> e
            }
        )
    }.flatMap {
        try {
            AppResult.Success(mappingFunction(it))
        } catch (e: Exception) {
            AppResult.Error(MappingException(e))
        }
    }

    companion object {
        private const val HTTP_CODE_SUCCESS = 200
    }
}