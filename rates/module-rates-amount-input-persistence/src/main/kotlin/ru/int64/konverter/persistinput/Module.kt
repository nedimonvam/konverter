package ru.int64.konverter.persistinput

import org.koin.android.ext.koin.androidContext
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.module
import ru.int64.konverter.interactor.repos.AmountInputRepo
import ru.int64.persistinput.repo.AmountInputDs
import ru.int64.persistinput.repo.AmountInputRepoImpl
import ru.int64.pesistinput.ds.AmountInputDsPrefsImpl

inline fun <reified T> modulePersistAmountInputFor() = module {
    single<AmountInputDs>(named<T>()) {
        AmountInputDsPrefsImpl(
            context = androidContext(),
            sharedPrefsName = T::class.java.simpleName
        )
    }
    single<AmountInputRepo>(named<T>()) { AmountInputRepoImpl(get(named<T>())) }
}
