package ru.int64.konverter.data

import android.content.Context
import android.net.ConnectivityManager
import ru.int64.lib.network.networkstate.NetworkConnectionDs

class NetworkConnectionDsImpl(
    private val context: Context
) : NetworkConnectionDs {

    override fun getIsConnected(): Boolean {
        val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        val activeNetworkInfo = manager?.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}