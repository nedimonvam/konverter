package ru.int64.konverter.data

import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import ru.int64.konverter.interactor.repos.RateRepo
import ru.int64.lib.network.networkstate.NetworkConnectionDs
import ru.int64.rateset.ds.RateApiDs
import ru.int64.rateset.ds.RatePersistenceDs
import ru.int64.rateset.ds.api.RateApiDsImpl
import ru.int64.rateset.ds.persistence.RatePersistenceDsImpl
import ru.int64.rateset.repo.RateRepoImpl

fun moduleRatesData(
    apiUrl: String,
    isLoggingEnabled: Boolean
) = module {
    single { HttpClientFactory.create(isLoggingEnabled) }
    single<RateApiDs> { RateApiDsImpl(apiUrl, get()) }
    single<RatePersistenceDs> { RatePersistenceDsImpl(get()) }
    single<RateRepo> { RateRepoImpl(get(), get(), get()) }
    single<NetworkConnectionDs> { NetworkConnectionDsImpl(androidContext()) }
}
