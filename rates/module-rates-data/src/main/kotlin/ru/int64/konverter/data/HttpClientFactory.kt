package ru.int64.konverter.data


import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

object HttpClientFactory {

    private const val TIMEOUT_DEFAULT = 10L

    fun create(
        isLoggingEnabled: Boolean,
        readTimeout: Long = TIMEOUT_DEFAULT,
        writeTimeout: Long = TIMEOUT_DEFAULT
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .readTimeout(readTimeout, TimeUnit.SECONDS)
            .writeTimeout(writeTimeout, TimeUnit.SECONDS)
            .apply {
                if (isLoggingEnabled) {
                    addInterceptor(HttpLoggingInterceptor().apply {
                        level = HttpLoggingInterceptor.Level.BODY
                    })
                }
            }
            .build()
    }
}