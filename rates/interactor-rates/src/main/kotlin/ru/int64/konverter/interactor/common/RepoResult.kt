package ru.int64.konverter.interactor.common

data class RepoResult<T>(
    val result: T,
    val dsType: DsType
) {
    enum class DsType { API, DB }
}