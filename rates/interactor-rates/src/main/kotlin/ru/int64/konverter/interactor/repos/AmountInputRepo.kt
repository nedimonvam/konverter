package ru.int64.konverter.interactor.repos

import ru.int64.rateset.Amount

interface AmountInputRepo {

    fun setInput(input: Amount)

    fun getInput(defaultAmount: Amount): Amount
}