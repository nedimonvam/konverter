package ru.int64.konverter.interactor.repos

import io.reactivex.Single
import ru.int64.konverter.interactor.common.RepoResult
import ru.int64.rateset.Currency
import ru.int64.rateset.RateSet

typealias RateSetResult = RepoResult<RateSet>

interface RateRepo {

    fun getRateSet(base: Currency): Single<RateSetResult>

    class DataNotLoadedException(cause: Throwable) : RuntimeException(cause)
}