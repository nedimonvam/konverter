package ru.int64.konverter.shared.ratefeatures.ui.ext

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE

fun <T : View> T.showIf(isVisible: Boolean) = apply {
    visibility = if (isVisible) VISIBLE else GONE
}