package ru.int64.konverter.shared.ratefeatures.ui.adapters

import android.annotation.SuppressLint
import androidx.recyclerview.widget.DiffUtil

abstract class SimpleDiffCallback<T : Any> : DiffUtil.ItemCallback<T>() {

    abstract fun getItemId(item: T): Any?

    override fun areItemsTheSame(
        oldItem: T,
        newItem: T
    ) = oldItem::class == newItem::class && getItemId(oldItem) == getItemId(newItem)

    /*
    T must be data class or [areContentsTheSame] must be overridden
    */
    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: T, newItem: T): Boolean {
        return oldItem == newItem
    }
}
