package ru.int64.konverter.shared.ratefeatures.ui.ext

import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE
import com.badoo.mvicore.binder.lifecycle.Lifecycle
import com.badoo.mvicore.binder.lifecycle.Lifecycle.Event.BEGIN
import com.badoo.mvicore.binder.lifecycle.Lifecycle.Event.END
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject


fun RecyclerView.scrollLifecycleEvents(): Observable<Lifecycle.Event> = BehaviorSubject
    .createDefault(BEGIN)
    .apply {
        addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) = onNext(
                if (newState == SCROLL_STATE_IDLE) BEGIN else END
            )
        })
    }