package ru.int64.konverter.shared.ratefeatures.ui.utils

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment

object SoftInputUtil {

    fun hideSoftKeyboard(view: View) {
        view.windowToken?.let { v ->
            val imm = view.context.getSystemService(
                Context.INPUT_METHOD_SERVICE
            ) as? InputMethodManager
            imm?.hideSoftInputFromWindow(v, 0)
            //imm?.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        }
    }

    fun hideSoftKeyboard(fragment: Fragment) {
        fragment.view?.let {
            hideSoftKeyboard(it)
        }
    }
}
