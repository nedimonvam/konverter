package ru.int64.konverter.shared.ratefeatures.ui.mvi.news

import android.util.Log
import io.reactivex.functions.Consumer
import ru.int64.konverter.converter.feature.ConverterFeature

class ErrorLogger(
    private val tag: String
) : Consumer<ConverterFeature.News.Error> {

    override fun accept(t: ConverterFeature.News.Error?) {
        Log.e(tag, "News.Error", t?.throwable)
    }
}