package ru.int64.konverter.shared.ratefeatures.ui.mvi.news

import ru.int64.konverter.converter.feature.ConverterFeature
import ru.int64.konverter.interactor.repos.RateRepo


class NewsToNewsActionTransformer : (ConverterFeature.News) -> NewsAction? {

    override fun invoke(news: ConverterFeature.News): NewsAction? = when (news) {
        is ConverterFeature.News.Error -> when (news.throwable) {
            // ignore Exceptions that are handled as network state
            is RateRepo.DataNotLoadedException -> null
            else -> when (val errorMessage = news.throwable.message) {
                null -> NewsAction.Snackbar.CommonError
                else -> NewsAction.Snackbar.ErrorMessage(errorMessage)
            }
        }
        is ConverterFeature.News.HideSoftInput -> NewsAction.HideSoftInput
    }
}