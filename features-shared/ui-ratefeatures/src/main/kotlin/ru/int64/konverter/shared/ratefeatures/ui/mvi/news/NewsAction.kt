package ru.int64.konverter.shared.ratefeatures.ui.mvi.news

sealed class NewsAction {
    sealed class Snackbar : NewsAction() {
        data class ErrorMessage(val message: String) : Snackbar()
        object CommonError : Snackbar()
    }
    object HideSoftInput : NewsAction()
}