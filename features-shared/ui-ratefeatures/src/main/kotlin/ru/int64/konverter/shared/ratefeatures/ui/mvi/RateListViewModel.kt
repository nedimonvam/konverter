package ru.int64.konverter.shared.ratefeatures.ui.mvi

abstract class RateListViewModel {
    abstract val loadingVisible: Boolean
    abstract val networkIndicatorVisible: Boolean
    abstract val networkOverlayVisible: Boolean
}