package ru.int64.konverter.shared.ratefeatures.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding

abstract class BaseRecyclerViewAdapter<T : Any>(
    diffCallback: DiffUtil.ItemCallback<T>
) : ListAdapter<T, BaseRecyclerViewAdapter.ViewHolder<T>>(diffCallback) {

    constructor(getItemId: (T) -> Any?) : this(object : SimpleDiffCallback<T>() {
        override fun getItemId(item: T) = getItemId(item)
    })

    abstract fun inflateBinding(
        layoutInflater: LayoutInflater,
        viewType: Int,
        parent: ViewGroup,
        attachToRoot: Boolean
    ): ViewBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder<T> {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = inflateBinding(
            layoutInflater,
            viewType,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(
        holder: ViewHolder<T>,
        position: Int
    ) {
        holder.model = getItem(position)
        bind(holder, position)
    }

    override fun onBindViewHolder(
        holder: ViewHolder<T>,
        position: Int,
        payloads: MutableList<Any>
    ) {
        holder.model = getItem(position)
        bind(holder, position)
    }

    private fun bind(holder: ViewHolder<T>, position: Int) {
        holder.model?.let { item ->
            bind(item, holder.binding, holder.context, getItemViewType(position))
        }
    }

    /**
     * override this method for binding
     */
    abstract fun bind(item: T, binding: ViewBinding, context: Context, itemViewType: Int)

    open class ViewHolder<T>(
        val binding: ViewBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        val context: Context get() = itemView.context
        var model: T? = null
    }

}
