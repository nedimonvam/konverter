package ru.int64.konverter.shared.ratefeatures.ui.mvi

import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import io.reactivex.functions.Consumer
import ru.int64.konverter.shared.ratefeatures.ui.R
import ru.int64.konverter.shared.ratefeatures.ui.mvi.news.NewsAction
import ru.int64.konverter.shared.ratefeatures.ui.utils.SoftInputUtil

open class CommonNewsConsumer(private val fragment: Fragment) : Consumer<NewsAction> {

    override fun accept(news: NewsAction?) {
        when (news) {
            is NewsAction.Snackbar -> {
                fragment.view?.let {
                    Snackbar.make(
                        it,
                        when (news) {
                            is NewsAction.Snackbar.ErrorMessage -> news.message
                            is NewsAction.Snackbar.CommonError -> fragment.getString(R.string.message_error_common)
                        },
                        Snackbar.LENGTH_LONG
                    ).show()
                }
            }
            is NewsAction.HideSoftInput -> SoftInputUtil.hideSoftKeyboard(fragment)
        }
    }
}