package ru.int64.konverter.shared.ratefeatures.ui.ext

import android.view.View.GONE
import android.view.View.VISIBLE

fun Boolean.toVisibleOrGone() = if (this) VISIBLE else GONE