package ru.int64.konverter.shared.flags

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class CurrencyResourceGroup(
    val code: String,
    @StringRes val nameResource: Int,
    @DrawableRes val roundIconResource: Int
)