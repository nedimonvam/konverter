package ru.int64.konverter.shared.flags

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.annotation.VisibleForTesting
import java.util.*

object CurrencyMapper {

    /** all country code supposed to be English abbreviations
     * codes are case insensitive
     * @see [getResourcesOrNull]
     *
     * Codes are supposed to be lower case strings internally within this mapper
     */
    private val locale = Locale.ENGLISH

    fun getSupportedCountyCodes() = resourceMap.keys.toSet()

    fun getCurrencyNameResOrNull(
        countryCode: String
    ): Int? = getResourcesOrNull(countryCode)?.nameResource

    fun getCountryRoundIconResOrNull(
        countryCode: String
    ): Int? = getResourcesOrNull(countryCode)?.roundIconResource

    @VisibleForTesting
    val resourceGroups = listOf(
        groupOf("aud", R.string.currency_name_aud, R.drawable.ic_aud),
        groupOf("eur", R.string.currency_name_eur, R.drawable.ic_eur),
        groupOf("bgn", R.string.currency_name_bgn, R.drawable.ic_bgn),
        groupOf("brl", R.string.currency_name_brl, R.drawable.ic_brl),
        groupOf("cad", R.string.currency_name_cad, R.drawable.ic_cad),
        groupOf("chf", R.string.currency_name_chf, R.drawable.ic_chf),
        groupOf("cny", R.string.currency_name_cny, R.drawable.ic_cny),
        groupOf("czk", R.string.currency_name_czk, R.drawable.ic_czk),
        groupOf("dkk", R.string.currency_name_dkk, R.drawable.ic_dkk),
        groupOf("gbp", R.string.currency_name_gbp, R.drawable.ic_gbp),
        groupOf("hkd", R.string.currency_name_hkd, R.drawable.ic_hkd),
        groupOf("hrk", R.string.currency_name_hrk, R.drawable.ic_hrk),
        groupOf("huf", R.string.currency_name_huf, R.drawable.ic_huf),
        groupOf("idr", R.string.currency_name_idr, R.drawable.ic_idr),
        groupOf("ils", R.string.currency_name_ils, R.drawable.ic_ils),
        groupOf("inr", R.string.currency_name_inr, R.drawable.ic_inr),
        groupOf("isk", R.string.currency_name_isk, R.drawable.ic_isk),
        groupOf("jpy", R.string.currency_name_jpy, R.drawable.ic_jpy),
        groupOf("krw", R.string.currency_name_krw, R.drawable.ic_krw),
        groupOf("mxn", R.string.currency_name_mxn, R.drawable.ic_mxn),
        groupOf("myr", R.string.currency_name_myr, R.drawable.ic_myr),
        groupOf("nok", R.string.currency_name_nok, R.drawable.ic_nok),
        groupOf("nzd", R.string.currency_name_nzd, R.drawable.ic_nzd),
        groupOf("php", R.string.currency_name_php, R.drawable.ic_php),
        groupOf("pln", R.string.currency_name_pln, R.drawable.ic_pln),
        groupOf("ron", R.string.currency_name_ron, R.drawable.ic_ron),
        groupOf("sek", R.string.currency_name_sek, R.drawable.ic_sek),
        groupOf("sgd", R.string.currency_name_sgd, R.drawable.ic_sgd),
        groupOf("thb", R.string.currency_name_thb, R.drawable.ic_thb),
        groupOf("zar", R.string.currency_name_zar, R.drawable.ic_zar),
        groupOf("usd", R.string.currency_name_usd, R.drawable.ic_usd),
        groupOf("rub", R.string.currency_name_rub, R.drawable.ic_rub)
    )

    private val resourceMap = hashMapOf(
        *(resourceGroups.map { it.code to it }).toTypedArray()
    )

    private fun groupOf(
        code: String,
        @StringRes nameResource: Int,
        @DrawableRes roundIconResource: Int
    ) = CurrencyResourceGroup(code, nameResource, roundIconResource)

    private fun getResourcesOrNull(countryCode: String) = resourceMap[countryCode.toLowerCase(locale)]
}