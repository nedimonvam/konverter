package ru.int64.konverter.shared.flags

import ru.int64.rateset.Currency

fun Currency.getNameResIdOrNull(): Int? = CurrencyMapper.getCurrencyNameResOrNull(this.code)

fun Currency.getRoundIconResIdOrNull(): Int? =
    CurrencyMapper.getCountryRoundIconResOrNull(this.code)