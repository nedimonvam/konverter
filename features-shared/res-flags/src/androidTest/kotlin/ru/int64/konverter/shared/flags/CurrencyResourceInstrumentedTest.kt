package ru.int64.konverter.shared.flags

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import ru.int64.rateset.ext.currencyOf

@Suppress("UsePropertyAccessSyntax")
@RunWith(AndroidJUnit4::class)
class CurrencyResourceInstrumentedTest {

    private val appContext = InstrumentationRegistry.getInstrumentation().targetContext

    /**
     * Check that there are no duplicates in [CurrencyMapper.resourceGroups]
     * by [CurrencyResourceGroup.code] field
     */
    @Test
    fun checkNoDuplicatesInResourceGroups() =
        CurrencyMapper.resourceGroups
            .groupBy { it.code }
            .filter { it.value.size > 1 }
            .map { it.key }
            .let { duplicates ->
                assertThat(duplicates)
                    .`as`("CurrencyMapper.resourceGroups has duplicate currency codes $duplicates")
                    .isEmpty()
            }


    /**
     * Check that all extensions
     */
    @Test
    fun getNameResIdOrNullIsNotNullForAllSupportedCodes() {
        CurrencyMapper.getSupportedCountyCodes().forEach { code ->
            val resId = currencyOf(code).getNameResIdOrNull()
            assertThat(resId)
                .`as`("Name res ID for code '$code' returned null. But '$code' is listed in getSupportedCountyCodes()")
                .isNotNull()
            resId as Int

            val stringResourceValue = appContext.getString(resId)
            assertThat(stringResourceValue)
                .`as`("String resource for code '$code' returned null. But '$code' is listed in getSupportedCountyCodes()")
                .isNotNull()

        }
    }

    /**
     * Check that all extensions
     * [CurrencyMapper.resourceGroups - duplicate currency codes EUR]
     */
    @Test
    fun getRoundIconResIdOrNullIsNotNullForAllSupportedCodes() {
        CurrencyMapper.getSupportedCountyCodes().forEach { code ->
            val resId = currencyOf(code).getRoundIconResIdOrNull()
            assertThat(resId)
                .`as`("Round icon res ID for code '$code' returned null. But '$code' is listed in getSupportedCountyCodes()")
                .isNotNull()
            resId as Int

            val stringResourceValue = appContext.getString(resId)
            assertThat(stringResourceValue)
                .`as`("Round icon res for code '$code' returned null. But '$code' is listed in getSupportedCountyCodes()")
                .isNotNull()

        }
    }
}
