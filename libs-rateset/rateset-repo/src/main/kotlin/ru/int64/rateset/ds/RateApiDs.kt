package ru.int64.rateset.ds

import ru.int64.lib.appresult.AppResult
import ru.int64.rateset.Currency
import ru.int64.rateset.RateSet

interface RateApiDs {
    fun getRateSet(baseCurrency: Currency): AppResult<RateSet>
}