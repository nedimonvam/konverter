package ru.int64.rateset.repo

import io.reactivex.Single
import ru.int64.konverter.interactor.common.RepoResult
import ru.int64.konverter.interactor.repos.RateRepo
import ru.int64.konverter.interactor.repos.RateSetResult
import ru.int64.lib.appresult.ext.onError
import ru.int64.lib.appresult.ext.onSuccess
import ru.int64.lib.network.NetworkException
import ru.int64.lib.network.networkstate.NetworkConnectionDs
import ru.int64.rateset.Currency
import ru.int64.rateset.ds.RateApiDs
import ru.int64.rateset.ds.RatePersistenceDs

class RateRepoImpl(
    private val rateApiDs: RateApiDs,
    private val ratePersistenceDs: RatePersistenceDs,
    private val networkConnectionDs: NetworkConnectionDs
) : RateRepo {

    override fun getRateSet(base: Currency): Single<RateSetResult> =
        if (networkConnectionDs.getIsConnected()) {
            getFromApiDs(base).onErrorResumeNext {
                when (it) {
                    is NoSuchElementException,
                    is NetworkException -> getFromPersistenceDs(base)
                    else -> Single.error(it)
                }
            }
        } else {
            getFromPersistenceDs(base)
        }

    private fun getFromApiDs(base: Currency) = Single.create<RateSetResult> { emitter ->
        rateApiDs
            .getRateSet(base)
            .onError { emitter.tryOnError(it.throwable) }
            .onSuccess {
                ratePersistenceDs.saveRateSet(it)
                emitter.onSuccess(RateSetResult(it, RepoResult.DsType.API))
            }
    }

    private fun getFromPersistenceDs(base: Currency): Single<RateSetResult> =
        ratePersistenceDs
            .getRateSet(base)
            .map { RateSetResult(it, RepoResult.DsType.DB) }
            .toSingle()
            .onErrorResumeNext {
                when (it) {
                    is NoSuchElementException -> Single.error(RateRepo.DataNotLoadedException(it))
                    else -> Single.error(it)
                }
            }
}