package ru.int64.rateset.ds

import io.reactivex.Maybe
import ru.int64.rateset.Currency
import ru.int64.rateset.RateSet

interface RatePersistenceDs {

    fun getRateSet(baseCurrency: Currency): Maybe<RateSet>

    fun saveRateSet(rateSet: RateSet)
}