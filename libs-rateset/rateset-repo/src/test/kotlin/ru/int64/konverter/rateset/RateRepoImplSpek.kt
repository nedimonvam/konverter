package ru.int64.konverter.rateset

import io.mockk.every
import io.mockk.mockk
import io.reactivex.observers.TestObserver
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import ru.int64.konverter.interactor.common.RepoResult
import ru.int64.konverter.interactor.repos.RateSetResult
import ru.int64.lib.appresult.AppResult
import ru.int64.lib.network.networkstate.NetworkConnectionDs
import ru.int64.rateset.RateSetBuilder
import ru.int64.rateset.ds.RateApiDs
import ru.int64.rateset.ds.RatePersistenceDs
import ru.int64.rateset.ext.currencyOf
import ru.int64.rateset.ext.rateTo
import ru.int64.rateset.repo.RateRepoImpl
import java.math.BigDecimal
import java.util.concurrent.TimeUnit

val eur = currencyOf("EUR")
val rub = currencyOf("RUB")
val usd = currencyOf("USD")
val rateEurUsd = eur.rateTo(usd, BigDecimal(1.142))
val rateEurRub = eur.rateTo(rub, BigDecimal(75.26))
val rateSet = RateSetBuilder()
    .base(eur)
    .rates(listOf(rateEurUsd, rateEurRub))
    .build()

val rateSetApiResult = RateSetResult(rateSet, RepoResult.DsType.API)
val rateSetDbResult = RateSetResult(rateSet, RepoResult.DsType.DB)

object RateRepoImplSpek : Spek({
    val testObserver by memoized { TestObserver.create<RateSetResult>() }

    describe("RateRepo") {

        context("if ds returns Success") {
            val repo by memoized { // try to move up
                RateRepoImpl(
                    mockk<RateApiDs>().apply {
                        every { getRateSet(eur) } returns AppResult.Success(rateSet)
                    },
                    mockk<RatePersistenceDs>().apply {
                        every { saveRateSet(rateSet) } returns Unit
                    },
                    mockNetworkStateDs(true)
                )
            }

            it("repo should emit RateSetResult") {
                repo.getRateSet(eur).toObservable().subscribe(testObserver)
                with(testObserver) {
                    awaitTerminalEvent(1, TimeUnit.SECONDS)
                    assertValue(rateSetApiResult)
                }
            }
        }

        context("if ds returns Error") {
            val exception = Exception("exception message")
            val repo by memoized {
                RateRepoImpl(
                    mockk<RateApiDs>().apply {
                        every { getRateSet(eur) } returns AppResult.Error(exception)
                    },
                    mockk(),
                    mockNetworkStateDs(true)
                )
            }

            it("repo should emit Exception") {
                testObserver.run {
                    repo.getRateSet(eur).toObservable().subscribe(this)
                    awaitTerminalEvent(1, TimeUnit.SECONDS)
                    assertError(exception)
                }
            }
        }
    }
})

private fun mockNetworkStateDs(result: Boolean): NetworkConnectionDs {
    return mockk<NetworkConnectionDs>().apply {
        every { getIsConnected() } returns result
    }
}