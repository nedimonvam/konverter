package ru.int64.rateset.ds.persistence

import android.content.Context
import com.google.gson.Gson
import io.reactivex.Maybe
import ru.int64.rateset.Currency
import ru.int64.rateset.RateSet
import ru.int64.rateset.ds.RatePersistenceDs
import ru.int64.rateset.ds.persistence.model.RateSetDto
import ru.int64.rateset.ds.persistence.model.toDto
import ru.int64.rateset.ds.persistence.model.toEntity

class RatePersistenceDsImpl(
    private val context: Context,
    private val filename: String = FILENAME_DEFAULT
) : RatePersistenceDs {

    private val gson = Gson()

    override fun getRateSet(baseCurrency: Currency): Maybe<RateSet> {
        return Maybe.create { emitter ->
            val file = context.getFileStreamPath(filename)
            if (file.exists()) {
                emitter.onSuccess(
                    gson.fromJson(
                        file.bufferedReader(),
                        RateSetDto::class.java
                    ).toEntity()
                )
            } else {
                emitter.onComplete()
            }
        }
    }

    override fun saveRateSet(rateSet: RateSet) = context
        .getFileStreamPath(filename)
        .writeText(gson.toJson(rateSet.toDto()))

    companion object {
        const val FILENAME_DEFAULT = "rateset.json"
    }
}