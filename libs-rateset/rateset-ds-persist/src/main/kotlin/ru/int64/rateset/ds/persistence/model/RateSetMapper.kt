package ru.int64.rateset.ds.persistence.model

import ru.int64.rateset.RateSet
import ru.int64.rateset.RateSetBuilder
import ru.int64.rateset.ext.currencyOf
import ru.int64.rateset.ext.ratesTo

fun RateSet.toDto() = RateSetDto(
    baseCurrencyCode = base.code,
    ratesMap = rates.map { it.dividend.code to it.ratio }
        .toMap()
)

fun RateSetDto.toEntity(): RateSet {
    val base = currencyOf(baseCurrencyCode)
    return RateSetBuilder()
        .base(base)
        .rates(base.ratesTo(ratesMap.mapKeys { currencyOf(it.key) }))
        .build()
}