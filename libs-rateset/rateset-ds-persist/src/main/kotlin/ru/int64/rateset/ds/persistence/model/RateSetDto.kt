package ru.int64.rateset.ds.persistence.model

import java.math.BigDecimal

data class RateSetDto(
    val baseCurrencyCode: String,
    val ratesMap: Map<String, BigDecimal>
)