package ru.int64.rateset.ds

import io.mockk.every
import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import ru.int64.rateset.ext.currencyOf
import ru.int64.rateset.ext.rateTo
import ru.int64.lib.appresult.ext.valueOrNull
import ru.int64.lib.network.HttpResponseCodeException
import ru.int64.lib.network.MappingException
import ru.int64.lib.network.networkstate.NetworkConnectionDs
import ru.int64.rateset.ds.api.RateApiDsImpl
import ru.int64.rateset.ds.testutils.*
import java.math.BigDecimal

private val eur = currencyOf("EUR")
private val usd = currencyOf("USD")
private val rub = currencyOf("RUB")

private val eurToUsd = eur.rateTo(usd, BigDecimal(1.142))
private val eurToRub = eur.rateTo(rub, BigDecimal(75.26))

class RateDsApiImplSpec : Spek({

    val networkStateDs = mockk<NetworkConnectionDs>()
    every { networkStateDs.getIsConnected() } returns true

    /**
     * local fun that
     * @return RateDs that always returns result parsed from the provided
     * @param responseString
     */
    fun createDs(responseString: String): RateApiDs =
        RateApiDsImpl(
            "https://url.com",
            createMockClient(MockInterceptor(responseString))
        )

    describe("RateDsApiImpl") {

        describe("getRateSet method") {

            context("when API HTTP response code == 200") {

                /**
                 * @param input raw string of API response
                 * @return RateSet parsed from the raw string
                 */
                fun invokeGetRateSet(input: String) = createDs(input).getRateSet(eur)

                describe("response content") {

                    context("is VALID json") {

                        context("API returns incorrect response: no baseCurrency") {
                            val result = invokeGetRateSet(
                                """
                                {
                                    "rates": {
                                        "${usd.code}": ${eurToUsd.ratio},
                                        "${rub.code}": ${eurToRub.ratio}
                                    }}"""
                            )
                            itIsError(result)
                            itHasThrowable(
                                result,
                                type = MappingException::class,
                                message = "no baseCurrency"
                            )
                        }

                        context("API returns incorrect response: no rates") {
                            val result = invokeGetRateSet(
                                """
                                {
                                    "baseCurrency": "${eur.code}"
                                }"""
                            )
                            itIsError(result)
                            itHasThrowable(
                                result,
                                type = MappingException::class,
                                message = "no rates"
                            )
                        }

                        context("API returns fully valid response with 2 rates") {
                            val result = invokeGetRateSet(
                                """ 
                                {
                                    "baseCurrency": "${eur.code}",
                                    "rates": {
                                        "${usd.code}": ${eurToUsd.ratio},
                                        "${rub.code}": ${eurToRub.ratio}
                                }}"""
                            )

                            itIsSuccess(result)
                            result.valueOrNull?.let { rateSet ->
                                it("returns RateSet with correct base currency") {
                                    assertThat(rateSet.base).isEqualTo(eur)
                                }
                                it("returns RateSet with 2 provided rates inside and only them") {
                                    assertThat(rateSet.rates).hasSize(2)
                                    assertThat(rateSet.rates).contains(
                                        eurToUsd,
                                        eurToRub
                                    )
                                }
                                println("output:\n$rateSet")
                            }
                        }
                    }
                }
            }

            context("when API HTTP response code != 200") {
                // check that none of the code groups can be 'swallowed'
                listOf(
                    100 to "Continue",
                    204 to "No Content",
                    301 to "Moved Permanently",
                    404 to "Not found",
                    500 to "Internal Server Error"
                ).forEach { pair ->
                    val (responseCode, errorMessage) = pair
                    val ds = RateApiDsImpl(
                        "https://url.com",
                        createMockClient(
                            MockInterceptor(
                                "",
                                responseCode = responseCode,
                                responseMessage = errorMessage
                            )
                        )
                    )
                    val result = ds.getRateSet(eur)

                    itIsError(result)

                    itHasThrowable(
                        result,
                        type = HttpResponseCodeException::class,
                        message = responseCode.toString()
                    )
                }
            }
        }
    }
})