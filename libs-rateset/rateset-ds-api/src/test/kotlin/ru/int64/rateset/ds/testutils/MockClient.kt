package ru.int64.rateset.ds.testutils

import okhttp3.OkHttpClient

fun createMockClient(interceptor: MockInterceptor): OkHttpClient = OkHttpClient.Builder()
    .apply { addInterceptor(interceptor) }
    .build()