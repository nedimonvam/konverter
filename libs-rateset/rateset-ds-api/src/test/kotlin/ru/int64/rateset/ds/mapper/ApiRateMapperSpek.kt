package ru.int64.rateset.ds.mapper

import org.assertj.core.api.Assertions.*
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import ru.int64.lib.network.MappingException
import ru.int64.rateset.ds.api.mapper.toEntity
import ru.int64.rateset.ds.api.model.ApiRateResponse
import java.math.BigDecimal

private val validRates = mapOf<String?, BigDecimal?>(
    "code1" to BigDecimal(1),
    "code2" to BigDecimal(2)
)

object ApiRateMapperSpek : Spek({

    describe("invalid ApiRateResponse to RateSet entity mapping attempt") {

        mapOf(
            // null fields in ApiRateResponse:
            ApiRateResponse(null, validRates) to "no baseCurrencyCode",
            ApiRateResponse(null, null)
                to "no baseCurrencyCode",
            ApiRateResponse("base", null)
                to "no rates",

            // empty fields in ApiRateResponse:
            ApiRateResponse("", validRates)
                to "no baseCurrencyCode",
            ApiRateResponse("base", mapOf())
                to "no rates",

            // null values in rates map:
            ApiRateResponse("base", mapOf(null to BigDecimal(1))) to "No rates provided",
            ApiRateResponse("base", mapOf("code" to null)) to "No rates provided",
            ApiRateResponse("base", mapOf(null to null))
                to "No rates provided",

            // empty currency code in rates map
            ApiRateResponse("base", mapOf("" to BigDecimal(1)))
                to "Currency code cannot be empty String",

            // base currency appears in rates map
            ApiRateResponse("base", mapOf("base" to BigDecimal(2)))
                to "invalid rate",

            // duplicate in rates map
            ApiRateResponse("base", mapOf("base" to BigDecimal(2)))
                to "divisible == dividend but ratio != 1"
        )
            .forEach {
                val (response, message) = it

                context("for $response") {

                    it("should throw MappingException") {
                        assertThatThrownBy { response.toEntity() }
                            .isInstanceOf(MappingException::class.java)
                    }

                    it("MappingException message should contain '$message'") {
                        assertThatThrownBy { response.toEntity() }
                            .hasMessageContaining(message)
                    }
                }
            }
    }

    describe("valid ApiRateResponse to RateSet entity mapping") {

        listOf(
            ApiRateResponse(
                "base",
                mapOf("code" to BigDecimal(1))
            ),
            ApiRateResponse(
                "base",
                mapOf("code" to BigDecimal(1))
            )
        )
            .forEach { response ->

                context("for $response") {

                    it("should throw no exception") {
                        assertThatCode { response.toEntity() }
                            .doesNotThrowAnyException()
                    }

                    it("should return RateSet with provided base currency code in upper case") {
                        assertThat(response.toEntity().base.code)
                            .isEqualTo(response.baseCurrencyCode?.toUpperCase())
                    }

                    it("should return RateSet with provided rates code in upper case") {
                        val rates = response.toEntity().rates
                        assertThat(rates.map {
                            it.dividend.code to it.ratio
                        }).isEqualTo(response.rates?.toList()?.map {
                            val (dividendCode, ratio) = it
                            dividendCode?.toUpperCase() to ratio
                        })
                    }
                }
            }
    }
})