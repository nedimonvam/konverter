package ru.int64.rateset.ds.testutils

import org.assertj.core.api.Assertions
import org.spekframework.spek2.style.specification.Suite
import ru.int64.lib.appresult.AppResult
import ru.int64.lib.appresult.ext.errorOrNull
import kotlin.reflect.KClass


fun Suite.itIsError(
    result: AppResult<*>
) = it("returns Error result") {
    Assertions.assertThat(result).isInstanceOf(AppResult.Error::class.java)
}

fun <T : Any> Suite.itHasThrowable(
    result: AppResult<*>,
    type: KClass<T>,
    message: String? = null
) = it("throwable is ${type.java.canonicalName}"
    + message?.let { "; message contains error code: $it" }
) {
    Assertions.assertThat(result.errorOrNull?.throwable).isInstanceOf(type.java)
    message?.let {
        Assertions.assertThat(result.errorOrNull?.throwable?.message?.toLowerCase())
            .contains(message.toLowerCase())
    }
}

fun Suite.itIsSuccess(result: AppResult<*>) =
    it("returns Success result") {
        Assertions.assertThat(result).isInstanceOf(AppResult.Success::class.java)
    }