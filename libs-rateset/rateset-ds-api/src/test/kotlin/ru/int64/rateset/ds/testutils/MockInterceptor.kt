package ru.int64.rateset.ds.testutils

import okhttp3.*
import java.io.IOException

class MockInterceptor(
    private val responseString: String,
    private val responseCode: Int = 200,
    private val responseMessage: String = "OK"
) : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        return Response.Builder()
            .code(responseCode)
            .request(chain.request())
            .protocol(Protocol.HTTP_1_0)
            .body(
                ResponseBody.create(
                    MediaType.parse("application/json"),
                    responseString.toByteArray()
                )
            )
            .message(responseMessage)
            .addHeader("content-type", "application/json")
            .build()
    }
}

