package ru.int64.rateset.ds.api.mapper

import ru.int64.lib.network.MappingException
import ru.int64.rateset.Currency
import ru.int64.rateset.Rate
import ru.int64.rateset.RateSet
import ru.int64.rateset.RateSetBuilder
import ru.int64.rateset.ds.api.model.ApiRateResponse
import ru.int64.rateset.ext.currencyOf
import ru.int64.rateset.ext.rateTo
import java.math.BigDecimal

fun ApiRateResponse.toEntity(): RateSet = try {
    when {
        baseCurrencyCode.isNullOrEmpty() -> throw MappingException("Cannot parse server response: no baseCurrencyCode")
        rates.isNullOrEmpty() -> throw MappingException("Cannot parse server response: no rates")
        else -> {
            val base = currencyOf(baseCurrencyCode)
            RateSetBuilder()
                .base(base)
                .rates(rates.toRateEntities(base))
                .build()
        }
    }
} catch (mappingException: MappingException) {
    throw mappingException
} catch (exception: Exception) {
    throw MappingException(exception)
}

fun Map<String?, BigDecimal?>.toRateEntities(base: Currency): List<Rate> = try {
    entries.mapNotNull {
        val (currencyCode, rateRatio) = it
        when {
            currencyCode == null ||
                rateRatio == null ||
                rateRatio <= BigDecimal.ZERO -> null
            else -> base.rateTo(currencyOf(currencyCode), rateRatio)
        }
    }
} catch (mappingException: MappingException) {
    throw mappingException
} catch (exception: Exception) {
    throw MappingException(exception)
}