package ru.int64.rateset.ds.api

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import ru.int64.lib.appresult.AppResult
import ru.int64.lib.network.BaseApiDs
import ru.int64.rateset.Currency
import ru.int64.rateset.RateSet
import ru.int64.rateset.ds.RateApiDs
import ru.int64.rateset.ds.api.mapper.toEntity
import ru.int64.rateset.ds.api.model.ApiRateResponse

class RateApiDsImpl(
    baseUrl: String,
    httpClient: OkHttpClient
) : BaseApiDs(), RateApiDs {

    private val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .client(httpClient)
            .addConverterFactory(
                GsonConverterFactory.create(
                    GsonBuilder()
                        .setLenient()
                        .create()
                )
            )
            .baseUrl(baseUrl)
            .build()
    }

    interface RateRetroService {
        @GET("api/android/latest")
        fun getRates(
            @Query("base") baseCurrencyCode: String
        ): Call<ApiRateResponse>
    }

    private val service by lazy { retrofit.create(RateRetroService::class.java) }

    override fun getRateSet(
        baseCurrency: Currency
    ): AppResult<RateSet> = runApiCall(service.getRates(baseCurrency.code)) {
        it.toEntity()
    }
}