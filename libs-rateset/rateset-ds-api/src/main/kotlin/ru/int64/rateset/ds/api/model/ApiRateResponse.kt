package ru.int64.rateset.ds.api.model

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class ApiRateResponse(
    @SerializedName("baseCurrency") val baseCurrencyCode: String? = null,
    @SerializedName("rates") val rates: Map<String?, BigDecimal?>? = null
)