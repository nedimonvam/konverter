package ru.int64.konverter.rateset.ext

import org.assertj.core.api.Assertions.assertThat
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

object CurrencyOfMethodSpek : Spek({

    describe("currencyOf global method") {
        val currencyCode = "EUR"
        val currency = ru.int64.rateset.ext.currencyOf(currencyCode)

        it("should return Currency with provided code") {
            assertThat(currency.code).isEqualTo(currencyCode)
        }
    }
})