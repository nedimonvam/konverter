package ru.int64.konverter.rateset

import org.assertj.core.api.Assertions.*
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import ru.int64.rateset.Currency
import ru.int64.rateset.Rate
import ru.int64.rateset.RateSet
import ru.int64.rateset.RateSetBuilder
import java.math.BigDecimal

private val rateEurRub = BigDecimal(75.26)
private val rateEurUsd = BigDecimal(1.142)
private val rateUsdToEur = BigDecimal(1).divide(rateEurUsd, RateSet.MATH_CONTEXT_DEFAULT)

private val eur = Currency.create("EUR")
private val rub = Currency.create("RUB")
private val usd = Currency.create("USD")

object RateSetBuilderSpek : Spek({

    describe("create RateSet using RateSetBuilder") {
        val base = eur
        val rates = listOf(
            Rate.create(eur, rub, rateEurRub),
            Rate.create(eur, usd, rateEurUsd)
        )

        context("build RateSet without rate list provided") {
            it("throws illegal state: no rates provided") {
                val builder = RateSetBuilder().base(base)
                // builder.rates() not called
                assertThatThrownBy { builder.build() }
                    .isInstanceOf(IllegalStateException::class.java)
                    .hasMessageContaining("No rates provided")
            }
        }

        context("with 2 Rates with the same divisible and different dividends (correct)") {
            val builder by memoized {
                RateSetBuilder()
                    .rates(rates)
            }

            context("build RateSet with both rate and base provided") {
                val rateSet by memoized { builder.base(base).build() }

                it("should build RateSet with provided base") {
                    assertThat(rateSet.base).isEqualTo(base)
                }

                it("should build RateSet with provided base") {
                    assertThat(rateSet.rates).isEqualTo(rates)
                }
            }

            context("build RateSet without base provided") {

                it("throws illegal state: no base provided") {
                    // builder.base() not called
                    assertThatThrownBy { builder.build() }
                        .isInstanceOf(IllegalStateException::class.java)
                        .hasMessageContaining("No base provided")
                }
            }
        }

        context("with 2 Rates with different divisible (incorrect: different base)") {
            val builder = RateSetBuilder()
                .base(base)
                .rates(listOf(
                    Rate.create(divisible = eur, dividend = rub, ratio = rateEurRub),
                    Rate.create(divisible = usd, dividend = eur, ratio = rateUsdToEur)
                             // divisibles are different
                ))

            it("should throw illegal state due to different base (divisible) currency") {
                assertThatThrownBy { builder.build() }
                    .isInstanceOf(IllegalStateException::class.java)
                    .hasMessageContaining("wrong divisible")
            }
        }

        context("with 2 Rates with the same divisible and dividend (incorrect: duplicates)") {
            val builder = RateSetBuilder()
                .base(base)
                .rates(listOf(
                    Rate.create(divisible = eur, dividend = rub, ratio = rateEurRub),
                    Rate.create(divisible = eur, dividend = rub, ratio = rateEurRub)
                             // duplicates
                ))

            it("throws illegal state due to duplicates in rates") {
                assertThatThrownBy { builder.build() }
                    .isInstanceOf(IllegalStateException::class.java)
                    .hasMessageContaining("duplicates")
            }
        }
    }
})