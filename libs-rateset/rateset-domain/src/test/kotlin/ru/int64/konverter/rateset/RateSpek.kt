package ru.int64.konverter.rateset

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import ru.int64.rateset.Amount
import ru.int64.rateset.Currency
import ru.int64.rateset.Rate
import ru.int64.rateset.RateSet
import java.math.BigDecimal
import java.math.RoundingMode


private val eur = Currency.create("EUR")
private val rub = Currency.create("RUB")

object RateSpek : Spek({

    describe("create Rate") {

        context("for 2 different currencies") {

            context("with negative ratio") {
                val negativeValue = BigDecimal(-1)
                it("should throw illegal argument") {
                    assertThatThrownBy { Rate.create(eur, rub, negativeValue) }
                        .isInstanceOf(IllegalArgumentException::class.java)
                        .hasMessageContaining("Rate ratio must be positive")
                }
            }

            context("with zero ratio") {
                val zeroValue = BigDecimal(0)
                it("should throw illegal argument") {
                    assertThatThrownBy { Rate.create(eur, rub, zeroValue) }
                        .isInstanceOf(IllegalArgumentException::class.java)
                        .hasMessageContaining("Rate ratio must be positive")
                }
            }

            context("positive ratio") {
                val positiveValue = BigDecimal(1)
                it("should create Rate with provided field values") {
                    with(Rate.create(eur, rub, positiveValue)) {
                        assertThat(divisible).isEqualTo(eur)
                        assertThat(dividend).isEqualTo(rub)
                        assertThat(ratio).isEqualTo(positiveValue)
                    }
                }
            }
        }

        context("for 2 equal currencies") {
            val eurCopy = Currency.create("EUR")

            context("with ratio = 1") {
                val valueOne = BigDecimal(1)
                it("should create Rate with provided field values") {
                    with(Rate.create(eur, eurCopy, valueOne)) {
                        assertThat(divisible).isEqualTo(eur)
                        assertThat(dividend).isEqualTo(eur)
                        assertThat(ratio).isEqualTo(valueOne)
                    }
                }
            }

            context("with ratio != 1") {
                val nonOneValue = BigDecimal(1.01)
                it("should throw Illegal Argument") {
                    assertThatThrownBy { Rate.create(eur, eurCopy, nonOneValue) }
                        .isInstanceOf(IllegalArgumentException::class.java)
                        .hasMessage("Attempt to create impossible Rate: divisible == dividend but ratio != 1")
                }
            }
        }
    }

    describe("convert Currencies") {

        context("using Rate EUR-to-RUB Rate") {
            val ratio = BigDecimal(82)
            val rate = Rate.create(eur, rub, ratio)

            context("correct input: convert 1 EUR") {
                val amountValue = BigDecimal(1)
                val oneEur = eur.createAmount(amountValue)
                val amountInRub = rate.calculate(oneEur)

                it("should return amount in RUB") {
                    assertThat(amountInRub.currency).isEqualTo(rub)
                }

                it("should return amount with value == ratio") {
                    assertThat(amountInRub.value).isEqualTo(ratio)
                }

                context("convert result back with RUB-to-EUR Rate") {
                    val revertedRate = Rate.create(
                        rub,
                        eur,
                        BigDecimal(1).divide(rate.ratio, RateSet.MATH_CONTEXT_DEFAULT)
                    )
                    val convertedBackAmountInEur = revertedRate.calculate(amountInRub)

                    it("should return amount in EUR") {
                        assertThat(convertedBackAmountInEur.currency).isEqualTo(eur)
                    }

                    it("should return amount of 1 with Rate's precision - 1") {
                        assertThat(
                            convertedBackAmountInEur.value.setScale(
                                revertedRate.ratio.precision() - 1,
                                RoundingMode.HALF_UP
                            ).stripTrailingZeros()
                        ).isEqualTo(amountValue)
                    }
                }
            }

            context("incorrect input: convert 1 USD") {
                val usd = Currency.create("USD")
                val amountInUsd = Amount.create(BigDecimal(1), usd)

                it("should throw illegal argument") {
                    assertThatThrownBy { rate.calculate(amountInUsd) }
                        .isInstanceOf(IllegalArgumentException::class.java)
                }
            }
        }
    }
})