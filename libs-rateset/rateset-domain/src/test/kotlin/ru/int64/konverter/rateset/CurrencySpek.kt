package ru.int64.konverter.rateset

import org.assertj.core.api.Assertions.*
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import ru.int64.rateset.Currency
import java.math.BigDecimal


object CurrencySpek : Spek({

    describe("create Currency") {

        context("with empty code") {
            val code = ""
            it("throws illegal argument: code cannot be empty") {
                assertThatThrownBy { Currency.create(code) }
                    .isInstanceOf(IllegalArgumentException::class.java)
                    .hasMessageContaining("code cannot be empty")
            }
        }

        context("with lower case code") {
            val code = "rus"
            it("Currency code should be upper case") {
                assertThat(Currency.create(code).code)
                    .isEqualTo(code.toUpperCase())
            }
        }
    }

    describe("compare 2 Currencies") {

        context("that differ only in case") {
            val lowerCaseCode = "rus"
            val upperCaseCode = "RUS"
            it("Currencies should be equal") {
                val currency1 = Currency.create(lowerCaseCode)
                val currency2 = Currency.create(upperCaseCode)
                assertThat(currency1).isEqualTo(currency2)
            }
        }

        context("that differ in symbols") {
            val rus = "rus"
            val eur = "eur"
            it("Currencies should not be equal") {
                val currency1 = Currency.create(rus)
                val currency2 = Currency.create(eur)
                assertThat(currency1).isNotEqualTo(currency2)
            }
        }
    }

    describe("create Amount from Currency") {
        val eur = Currency.create("EUR")
        context("amount of 1 of EUR Currency") {
            val value = BigDecimal(1)
            val amount = eur.createAmount(value)
            it("should have value 1") {
                assertThat(amount.value).isEqualTo(value)
            }
            it("should have currency EUR") {
                assertThat(amount.currency).isEqualTo(eur)
            }
        }
    }

})