package ru.int64.konverter.rateset.ext

import org.assertj.core.api.Assertions.assertThat
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import ru.int64.rateset.Currency
import ru.int64.rateset.ext.amountOf
import ru.int64.rateset.ext.rateTo
import ru.int64.rateset.ext.ratesTo
import java.math.BigDecimal

object AmountOfExtensionSpek : Spek( {

    describe("Float.amountOf extension") {
        val currency = Currency.create("code")
        val value = BigDecimal(1)

        describe("result Amount") {
            val amount = value amountOf currency

            it("should return Amount of provided Currency") {
                assertThat(amount.currency).isEqualTo(currency)
            }

            it("should return Amount with provided value") {
                assertThat(amount.value).isEqualTo(value)
            }
        }

        describe("Currency extensions") {

            describe("rateTo - create single rate") {
                val anotherCurrency = ru.int64.rateset.ext.currencyOf("RUB")
                val rateRatio = BigDecimal(85.12)
                val rate = currency.rateTo(anotherCurrency, rateRatio)

                describe("should return Rate") {

                    it("with divisible == Currency on which extension was called") {
                        assertThat(rate.divisible == currency)
                    }

                    it("with dividend == Currency provided in parameter") {
                        assertThat(rate.dividend == anotherCurrency)
                    }

                    it("with ratio value provided in parameter") {
                        assertThat(rate.ratio == rateRatio)
                    }
                }
            }

            describe("ratesTo - create multiple rate") {
                val anotherCurrency1 = ru.int64.rateset.ext.currencyOf("RUB")
                val rateRatio1 = BigDecimal(85.12)
                val anotherCurrency2 = ru.int64.rateset.ext.currencyOf("USD")
                val rateRatio2 = BigDecimal(1.123)

                describe("create from Currency to Float map") {
                    val map = mapOf(
                        anotherCurrency1 to rateRatio1,
                        anotherCurrency2 to rateRatio2
                    )
                    val rates = currency.ratesTo(map)

                    describe("should return List of Rates") {

                        it("with the same element numbers as provided Map") {
                            assertThat(rates).hasSameSizeAs(map.entries)
                        }

                        it("with each item divisible == Currency " +
                            "on which extension was called") {
                            assertThat(rates).allSatisfy {
                                assertThat(it.divisible).isEqualTo(currency)
                            }
                        }

                        it("with items dividends equal provided Map keys") {
                            assertThat(rates).allSatisfy {
                                assertThat(it.dividend).isIn(map.keys)
                            }
                        }

                        it("with items dividends equal provided Map values") {
                            assertThat(rates).allSatisfy {
                                assertThat(it.ratio).isIn(map.values)
                            }
                        }
                    }
                }
            }
        }
    }
})