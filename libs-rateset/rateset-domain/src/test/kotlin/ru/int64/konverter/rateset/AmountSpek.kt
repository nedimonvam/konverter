package ru.int64.konverter.rateset

import org.assertj.core.api.Assertions.assertThat
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import ru.int64.rateset.Amount
import ru.int64.rateset.Currency
import java.math.BigDecimal

object AmountSpek : Spek({

    describe("create Amount") {

        context("with any Currency and value") {
            val currency = Currency.create("EUR")
            val value = BigDecimal(123.45)

            it("creates Amount with provided Currency and Value") {
                val amount = Amount.create(value, currency)
                assertThat(amount.currency).isEqualTo(currency)
                assertThat(amount.value).isEqualTo(value)
            }
        }
    }
})