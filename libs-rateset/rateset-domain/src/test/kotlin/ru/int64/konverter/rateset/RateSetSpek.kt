package ru.int64.konverter.rateset

import org.assertj.core.api.Assertions.*
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import ru.int64.rateset.*
import java.math.BigDecimal

private val rateEurRub = BigDecimal(75.26)
private val rateEurUsd = BigDecimal(1.142)
private val eur = Currency.create("EUR")
private val rub = Currency.create("RUB")
private val usd = Currency.create("USD")

object RateSetSpek : Spek({

    val rateSet = RateSetBuilder()
        .base(eur)
        .rates(listOf(Rate.create(eur, rub, rateEurRub), Rate.create(eur, usd, rateEurUsd)))
        .build()

    describe("get Rate from RateSet") {

        context("for divisible Currency == dividend Currency") {

            it("ratio == 1") {
                rateSet.getRate(eur, eur).apply {
                    assertThat(ratio).isEqualTo(BigDecimal(1))
                }
            }
        }

        context("for pair that explicitly exists in RateSet") {

            it("should return Rate with the same ratio") {
                rateSet.getRate(eur, rub).apply {
                    assertThat(ratio).isEqualTo(rateEurRub)
                }
            }
        }

        context("for pair reverse to explicitly existing in RateSet") {

            it("should return Rate with reverted ratio of provided") {
                rateSet.getRate(rub, eur).apply {
                    assertThat(ratio).isEqualTo(BigDecimal(1).divide(rateEurRub, RateSet.MATH_CONTEXT_DEFAULT))
                }
            }
        }

        context("for pair of Currencies in RateSet without explicit rate pair") {

            it("should Rate with ratio calculated from 2 provided") {
                rateSet.getRate(rub, usd).apply {
                    assertThat(ratio).isEqualTo(rateEurUsd.divide(rateEurRub, RateSet.MATH_CONTEXT_DEFAULT))
                }
            }
        }

        context("for Currency which is unknown for RateSet") {
            val gbp = Currency.create("GBP")

            context("unknown-to-base") {

                it("throws illegal argument: not found") {
                    assertThatThrownBy { rateSet.getRate(gbp, eur) }
                        .isInstanceOf(IllegalArgumentException::class.java)
                        .hasMessageContaining("not found")
                }
            }

            context("base-to-unknown") {

                it("throws illegal argument: not found") {
                    assertThatThrownBy { rateSet.getRate(eur, gbp) }
                        .isInstanceOf(IllegalArgumentException::class.java)
                        .hasMessageContaining("not found")
                }
            }

            context("unknown-to-any") {

                it("throws illegal argument: not found") {
                    assertThatThrownBy { rateSet.getRate(gbp, rub) }
                        .isInstanceOf(IllegalArgumentException::class.java)
                        .hasMessageContaining("not found")
                }
            }

            context("any-to-unknown") {

                it("throws illegal argument: not found") {
                    assertThatThrownBy { rateSet.getRate(rub, gbp) }
                        .isInstanceOf(IllegalArgumentException::class.java)
                        .hasMessageContaining("not found")
                }
            }

            context("to any other Currency") {

                it("throws illegal argument: not found") {
                    assertThatThrownBy { rateSet.getRate(gbp, eur) }
                        .isInstanceOf(IllegalArgumentException::class.java)
                        .hasMessageContaining("not found")
                }
            }

            context(
                "for divisible Currency  == dividend Currency " +
                    "(== currency not in the RateSet)"
            ) {

                it("ratio == 1 (nevertheless Rate not presented in RateSet)") {
                    rateSet.getRate(gbp, gbp).apply {
                        assertThat(ratio).isEqualTo(BigDecimal(1))
                    }
                }
            }
        }
    }

    describe("calculate currency Amount using RateSet") {

        context(
            "from one Currency to another destination Currency, " +
                "both exist in RateSet"
        ) {
            val input = Amount.create(BigDecimal(12.34), eur)
            val destinationCurrency = usd

            it("should return Amount in destination currency") {
                rateSet.calculate(input, destinationCurrency).apply {
                    assertThat(currency).isEqualTo(destinationCurrency)
                }
            }

            it("should return Amount with value = input value * Rate ratio") {
                rateSet.calculate(input, destinationCurrency).apply {
                    assertThat(value).isEqualTo(input.value * rateEurUsd)
                }
            }
        }
    }
})