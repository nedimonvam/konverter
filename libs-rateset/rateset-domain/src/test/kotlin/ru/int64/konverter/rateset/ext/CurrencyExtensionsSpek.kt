package ru.int64.konverter.rateset.ext

import org.assertj.core.api.Assertions.assertThat
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import ru.int64.rateset.Currency
import ru.int64.rateset.ext.rateTo
import ru.int64.rateset.ext.ratesTo

object CurrencyExtensionsSpek : Spek({

    describe("Currency extensions") {
        val currency = Currency.create("code1")

        describe("rateTo - create single rate") {
            val anotherCurrency = Currency.create("another")
            val rateRatio = 12.34.toBigDecimal()
            val rate = currency.rateTo(anotherCurrency, rateRatio)

            describe("should return Rate") {

                it("with divisible == Currency on which extension was called") {
                    assertThat(rate.divisible == currency)
                }

                it("with dividend == Currency provided in parameter") {
                    assertThat(rate.dividend == anotherCurrency)
                }

                it("with ratio value provided in parameter") {
                    assertThat(rate.ratio == rateRatio)
                }
            }
        }

        describe("ratesTo - create multiple rate") {
            val anotherCurrency1 = Currency.create("code2")
            val rateRatio1 = 12.34.toBigDecimal()
            val anotherCurrency2 = Currency.create("code3")
            val rateRatio2 = 1.23.toBigDecimal()

            describe("create from Map of Currency to Float") {
                val map = mapOf(
                    anotherCurrency1 to rateRatio1,
                    anotherCurrency2 to rateRatio2
                )
                val rates = currency.ratesTo(map)

                describe("should return List of Rates") {

                    it("with the same size as provided Map") {
                        assertThat(rates).hasSameSizeAs(map.entries)
                    }

                    it("with each item divisible == Currency " +
                        "on which extension was called") {
                        assertThat(rates).allSatisfy {
                            assertThat(it.divisible).isEqualTo(currency)
                        }
                    }

                    it("with items dividends equal provided Map keys") {
                        assertThat(rates).allSatisfy {
                            assertThat(it.dividend).isIn(map.keys)
                        }
                    }

                    it("with items dividends equal provided Map values") {
                        assertThat(rates).allSatisfy {
                            assertThat(it.ratio).isIn(map.values)
                        }
                    }
                }
            }

            describe("create from Array of Pairs of Currency to Float") {
                val pairList = arrayOf(
                    anotherCurrency1 to rateRatio1,
                    anotherCurrency2 to rateRatio2,
                    // duplicating item
                    anotherCurrency2 to rateRatio2
                )
                val rates = currency.ratesTo(*pairList)

                describe("should return List of Rates") {

                    it("with the size of unique Pairs in provided Array") {
                        assertThat(rates).hasSameSizeAs(pairList.distinct())
                    }

                    it("with each item divisible == Currency " +
                        "on which extension was called") {
                        assertThat(rates).allSatisfy {
                            assertThat(it.divisible).isEqualTo(currency)
                        }
                    }

                    it("with items dividends equal provided Map keys") {
                        assertThat(rates).allSatisfy { rate ->
                            assertThat(rate.dividend).isIn(pairList.map { pair -> pair.first })
                        }
                    }

                    it("with items dividends equal provided Map values") {
                        assertThat(rates).allSatisfy {
                            assertThat(it.ratio).isIn(pairList.map { pair -> pair.second })
                        }
                    }
                }
            }
        }
    }
})