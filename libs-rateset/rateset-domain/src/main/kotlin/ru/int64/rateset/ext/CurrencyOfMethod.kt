package ru.int64.rateset.ext

import ru.int64.rateset.Currency

/**
 * create currency
 */
fun currencyOf(currencyCode: String) = Currency.create(currencyCode)