package ru.int64.rateset

import ru.int64.rateset.RateSet.Companion.MATH_CONTEXT_DEFAULT
import java.math.MathContext

class RateSetBuilder {

    private var baseArg: Currency? = null
    private var ratesArg: List<Rate>? = null
    private var mathContextArg: MathContext = MATH_CONTEXT_DEFAULT

    fun base(baseCurrency: Currency) = apply { baseArg = baseCurrency }

    fun rates(rates: List<Rate>) = apply { ratesArg = rates }

    fun mathContext(mathContext: MathContext) = apply { mathContextArg = mathContext }

    fun build(): RateSet {
        val rates = ratesArg
        val base = baseArg
        return when {
            // 1 Simple checks
            rates.isNullOrEmpty() -> throw IllegalStateException("No rates provided, call rates() before build()")
            base == null -> throw  IllegalStateException("No base provided, call base() before build()")
            // 2 Other checks
            else -> {
                // find inappropriate items:
                // - different base
                val rateWithDifferentBase = rates.firstOrNull { it.divisible != baseArg }
                // - duplicates
                val rateDuplicate = rates
                    .groupBy { it.dividend }
                    .filter { it.value.size > 1 }
                    .toList()
                    .firstOrNull()

                when {
                    // 2.1 different base
                    rateWithDifferentBase != null -> throw  IllegalStateException(
                        "Rate $rateWithDifferentBase has wrong divisible. Divisible should be equal base ($base) "
                    )
                    // 2.2 duplicates
                    rateDuplicate != null -> throw IllegalStateException(
                        "Rates have duplicates for currency ${rateDuplicate.first}: ${rateDuplicate.second.size} items"
                    )
                    // 3 All checks passed
                    else -> RateSet(base, rates, mathContextArg)
                }
            }
        }
    }
}