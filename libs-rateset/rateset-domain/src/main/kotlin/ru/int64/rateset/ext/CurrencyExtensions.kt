package ru.int64.rateset.ext

import ru.int64.rateset.Currency
import ru.int64.rateset.Rate
import ru.int64.rateset.RateSet
import java.math.BigDecimal
import java.math.MathContext

/**
 * create single rate
 */
fun Currency.rateTo(
    dividend: Currency,
    value: BigDecimal,
    mathContext: MathContext = RateSet.MATH_CONTEXT_DEFAULT
) = Rate.create(this, dividend, value, mathContext)

/**
 * create multiple rates
 */
fun Currency.ratesTo(dividendToValueMap: Map<Currency, BigDecimal>) = dividendToValueMap.map {
    this.rateTo(it.key, it.value)
}

/**
 * create multiple rates excluding duplicates
 */
fun Currency.ratesTo(
    vararg dividendNameToValuePairs: Pair<Currency, BigDecimal>
) = ratesTo(dividendNameToValuePairs.toMap())