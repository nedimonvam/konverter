package ru.int64.rateset.ext

import ru.int64.rateset.Amount
import ru.int64.rateset.Currency
import java.math.BigDecimal

infix fun BigDecimal.amountOf(currency: Currency): Amount = currency.createAmount(this)

infix fun Int.amountOf(currency: Currency): Amount = BigDecimal(this) amountOf currency

infix fun Int.amountOf(
    currencyCode: String
): Amount = BigDecimal(this) amountOf currencyOf(currencyCode)