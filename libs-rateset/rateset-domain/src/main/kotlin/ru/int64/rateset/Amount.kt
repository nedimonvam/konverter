package ru.int64.rateset

import java.math.BigDecimal

/**
 * Amount of money - a float sum in specific [Currency]
 */
data class Amount internal constructor(
    val value: BigDecimal,
    val currency: Currency
) {

    companion object {
        fun create(value: BigDecimal, currency: Currency) =
            Amount(value, currency)
    }
}