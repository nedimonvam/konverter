package ru.int64.rateset

import java.math.BigDecimal
import java.util.*

/**
 * Currency descriptor for creating [Amount], [Rate], [RateSet]
 *
 * @param code should be String in English of any length case insensitive
 * Currency("EUR") == Currency("eur")
 *
 * Additional params such as full string name are supposed to be implemented as extensions
 * in the modules where they are required
 */
data class Currency internal constructor(
    val code: String
) {

    fun createAmount(value: BigDecimal) =
        Amount.create(value, this)

    companion object {
        fun create(code: String) = when {
            code.isEmpty() -> throw IllegalArgumentException("Currency code cannot be empty String")
            else -> Currency(code.toUpperCase(Locale.ENGLISH))
        }
    }
}