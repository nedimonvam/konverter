package ru.int64.rateset

import java.math.BigDecimal
import java.math.MathContext


/**
 * Single currency rate - [ratio] of [divisible] to [dividend]
 *
 * Example:
 * [divisible] / [dividend] = [ratio]
 *    EUR      /     USD    =   1.1
 */
data class Rate internal constructor(
    val divisible: Currency,
    val dividend: Currency,
    val ratio: BigDecimal,
    val mathContext: MathContext
) {

    /**
     * @return value in [dividend] units that equals [divisibleCurrencyAmount] in [divisible] units
     */
    fun calculate(divisibleCurrencyAmount: Amount) = when (divisibleCurrencyAmount.currency) {
        divisible -> Amount(
            divisibleCurrencyAmount.value.times(ratio),
            dividend
        )
        dividend -> Amount(
            divisibleCurrencyAmount.value.divide(ratio, mathContext),
            dividend
        )
        else -> throw IllegalArgumentException(
            "Attempt to calculate (${divisibleCurrencyAmount.currency}) " +
                "with Rate ($this)"
        )
    }

    companion object {
        fun create(
            divisible: Currency,
            dividend: Currency,
            ratio: BigDecimal,
            mathContext: MathContext = RateSet.MATH_CONTEXT_DEFAULT
        ) = when {
            ratio <= 0.toBigDecimal() -> throw IllegalArgumentException("Rate ratio must be positive (ratio = $ratio)")
            divisible == dividend &&
                ratio != BigDecimal(1) -> throw IllegalArgumentException(
                "Attempt to create impossible Rate: divisible == dividend but ratio != 1"
            )
            else -> Rate(divisible, dividend, ratio, mathContext)
        }
    }
}