package ru.int64.rateset

import java.math.BigDecimal
import java.math.MathContext

data class RateSet internal constructor(
    val base: Currency,
    val rates: List<Rate>,
    val mathContext: MathContext = MATH_CONTEXT_DEFAULT
) {

    fun getRate(
        divisible: Currency,
        dividend: Currency
    ): Rate {
        val notFoundException = IllegalArgumentException("Rate $divisible/$dividend not found")
        return when {
            // case 0: return 1 if divisible == dividend
            divisible == dividend -> Rate.create(
                divisible,
                dividend,
                1.toBigDecimal(),
                mathContext
            )
            // case 1: Something to Base -> return existing Rate
            divisible == base -> {
                rates.firstOrNull { it.dividend == dividend }
                    ?: throw notFoundException
            }
            // case 2: Base to Something -> return reverse Rate to existing
            dividend == base -> {
                // 2.1 find existing rate which is reverse to the requested
                rates.firstOrNull { it.dividend == divisible }?.let { reverseRate ->
                    // 2.2 reverse it back
                    Rate.create(
                        divisible,
                        dividend,
                        BigDecimal(1).divide(reverseRate.ratio, mathContext),
                        mathContext
                    )
                } ?: throw notFoundException
            }
            else -> {
                // Case 3: common case, calculate rate from 2 other rates via base currency
                val baseToDivisible = rates.firstOrNull { it.dividend == divisible }
                val baseToDividend = rates.firstOrNull { it.dividend == dividend }
                when {
                    baseToDivisible == null || baseToDividend == null -> throw notFoundException
                    else -> calculateRate(baseToDivisible, baseToDividend)
                }
            }
        }
    }

    fun calculate(
        inputAmount: Amount,
        destinationCurrency: Currency
    ) = getRate(divisible = inputAmount.currency, dividend = destinationCurrency)
        .calculate(inputAmount)
        .let { resultAmount ->
            Amount(
                resultAmount.value,
                destinationCurrency
            )
        }

    private fun calculateRate(
        baseToDivisible: Rate,
        baseToDividend: Rate
    ): Rate {
        if (baseToDivisible.divisible != baseToDividend.divisible) {
            // currently impossible case, does not require to be covered
            throw IllegalArgumentException("Only rates with the same divisible can be calculated")
        } else {
            return Rate.create(
                divisible = baseToDivisible.dividend,
                dividend = baseToDividend.dividend,
                ratio = baseToDividend.ratio.divide(baseToDivisible.ratio, mathContext),
                mathContext = mathContext
            )
        }
    }


    companion object {
        val MATH_CONTEXT_DEFAULT: MathContext = MathContext.DECIMAL32
    }
}