package ru.int64.konverter.viewer.viewmodel

import ru.int64.konverter.converter.feature.ConverterFeature
import ru.int64.konverter.converter.viewmodels.CurrencyItem

class StateToViewModelTransformer : (ConverterFeature.State) -> ViewModel {

    override fun invoke(state: ConverterFeature.State) = ViewModel(
        currencyItems = if (state.rateSet == null) {
            listOf()
        } else {
            state.amounts
                .sortedBy { if (it.currency == state.selectedCurrency) 0 else 1 }
                .map { CurrencyItem(it) }
        },
        loadingVisible = state.isLoading,
        networkIndicatorVisible = state.noConnection,
        networkOverlayVisible = state.noConnection && state.rateSet == null
    )
}