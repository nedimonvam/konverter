package ru.int64.konverter.viewer.fragment

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import ru.int64.konverter.converter.viewmodels.CurrencyItem
import ru.int64.konverter.shared.ratefeatures.ui.adapters.BaseRecyclerViewAdapter
import ru.int64.konverter.shared.ratefeatures.ui.databinding.ItemRateOverviewBinding

class ViewerRateListRecyclerAdapter :
    BaseRecyclerViewAdapter<CurrencyItem>({ it.amount.currency }) {

    override fun inflateBinding(
        layoutInflater: LayoutInflater,
        viewType: Int,
        parent: ViewGroup,
        attachToRoot: Boolean
    ): ItemRateOverviewBinding = ItemRateOverviewBinding
        .inflate(layoutInflater, parent, attachToRoot)

    override fun bind(
        item: CurrencyItem,
        binding: ViewBinding,
        context: Context,
        itemViewType: Int
    ) = OverviewRateItemViewBinder.bind(item, binding as ItemRateOverviewBinding, context)
}