package ru.int64.konverter.viewer.viewmodel

import ru.int64.konverter.converter.viewmodels.CurrencyItem
import ru.int64.konverter.shared.ratefeatures.ui.mvi.RateListViewModel


data class ViewModel(
    val currencyItems: List<CurrencyItem>,
    override val loadingVisible: Boolean,
    override val networkIndicatorVisible: Boolean,
    override val networkOverlayVisible: Boolean
) : RateListViewModel()