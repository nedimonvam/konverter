package ru.int64.konverter.viewer.fragment

import android.content.Context
import ru.int64.konverter.converter.viewmodels.CurrencyItem
import ru.int64.konverter.shared.flags.getNameResIdOrNull
import ru.int64.konverter.shared.flags.getRoundIconResIdOrNull
import ru.int64.konverter.shared.ratefeatures.ui.R
import ru.int64.konverter.shared.ratefeatures.ui.databinding.ItemRateOverviewBinding

object OverviewRateItemViewBinder {
    fun bind(
        item: CurrencyItem,
        binding: ItemRateOverviewBinding,
        context: Context
    ) {
        binding.apply {

            // bind simple fields
            txtCurrencyCode.text = item.amount.currency.code
            txtRateValue.text = context.getString(R.string.format_float_amount, item.amount.value)

            // bind resources from :lib-flags
            item.amount.currency.apply {
                getNameResIdOrNull()?.let { txtCurrencyName.text = context.getString(it) }
                getRoundIconResIdOrNull()?.let { imgCountryFlag.setImageDrawable(context.getDrawable(it)) }
            }
        }


    }
}