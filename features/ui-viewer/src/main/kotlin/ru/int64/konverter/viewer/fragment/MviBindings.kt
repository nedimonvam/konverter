package ru.int64.konverter.viewer.fragment

import com.badoo.mvicore.binder.lifecycle.Lifecycle
import com.badoo.mvicore.binder.using
import io.reactivex.Observable
import io.reactivex.functions.Consumer
import ru.int64.konverter.converter.feature.ConverterFeature
import ru.int64.konverter.shared.ratefeatures.ui.mvi.news.ErrorLogger
import ru.int64.konverter.shared.ratefeatures.ui.mvi.news.NewsAction
import ru.int64.konverter.shared.ratefeatures.ui.mvi.news.NewsToNewsActionTransformer
import ru.int64.konverter.viewer.viewmodel.StateToViewModelTransformer
import ru.int64.konverter.viewer.viewmodel.ViewModel
import ru.int64.lib.lib.mvi.lifecycle.ext.foldLifecycleSources
import ru.int64.lib.mvilifecycle.android.bindings.BinderBehavior
import ru.int64.lib.mvilifecycle.android.bindings.GenericAndroidLifecycleFeatureBindings
import ru.int64.lib.mvilifecycle.android.ext.getLifecycleEventSource

class MviBindings(
    view: ViewerFeatureFragment,
    private val feature: ConverterFeature,
    viewModelConsumer: Consumer<ViewModel>,
    newsActionConsumer: Consumer<NewsAction>,
    lifecycleEventSources: List<Observable<Lifecycle.Event>> = listOf()
) : GenericAndroidLifecycleFeatureBindings<Nothing, ViewModel, NewsAction>(
    feature,
    viewLifecycleEvents = foldLifecycleSources(
        lifecycleEventSources + view.viewLifecycleOwner.getLifecycleEventSource(BinderBehavior.ResumePause)
    ),
    coreLifecycleEvents = foldLifecycleSources(
        listOf(view.viewLifecycleOwner.getLifecycleEventSource(BinderBehavior.ResumePause))
    ),
    setup = { viewBinder ->
        viewBinder.run {
            // feature -> view
            bind(feature to viewModelConsumer using StateToViewModelTransformer())
            bind(feature.news to newsActionConsumer using NewsToNewsActionTransformer())
            bind(feature.news to ErrorLogger("Viewer") using { it as? ConverterFeature.News.Error })
        }
    }
)