package ru.int64.konverter.converter.feature

import com.badoo.mvicore.element.NewsPublisher
import ru.int64.konverter.converter.feature.ConverterFeature.Effect
import ru.int64.konverter.converter.feature.ConverterFeature.News
import ru.int64.lib.network.NetworkException
import java.util.concurrent.TimeoutException

class NewsPublisherImpl : NewsPublisher<ConverterFeature.Wish, Effect, ConverterFeature.State, News> {

    /**
     * ignore [Effect.RateSetLoadingError] with [TimeoutException] and [TimeoutException]
     * since network state is observed and included into [ConverterFeature.State]
     */
    override fun invoke(
        wish: ConverterFeature.Wish,
        effect: Effect,
        state: ConverterFeature.State
    ): News? = when (effect) {
        is Effect.RateSetLoadingError -> when (effect.throwable) {
            is NetworkException -> null
            else -> News.Error(effect.throwable)
        }
        is Effect.HideSoftInput -> News.HideSoftInput
        else -> null
    }
}