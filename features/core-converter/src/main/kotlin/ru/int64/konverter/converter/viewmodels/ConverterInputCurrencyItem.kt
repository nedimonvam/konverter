package ru.int64.konverter.converter.viewmodels

import ru.int64.rateset.Amount

class ConverterInputCurrencyItem(
    amount: Amount,
    val isActive: Boolean
) : CurrencyItem(amount) {
    val currency = amount.currency
}