package ru.int64.konverter.converter.feature

import com.badoo.mvicore.binder.lifecycle.Lifecycle
import io.reactivex.Scheduler
import io.reactivex.subjects.BehaviorSubject
import ru.int64.konverter.converter.feature.ConverterFeature.*
import ru.int64.konverter.interactor.repos.AmountInputRepo
import ru.int64.konverter.interactor.repos.RateRepo
import ru.int64.lib.lib.mvi.lifecycle.ActorReducerLifecycleFeature
import ru.int64.rateset.Amount
import ru.int64.rateset.Currency
import ru.int64.rateset.RateSet
import java.math.BigDecimal

class ConverterFeature private constructor(
    rateRepo: RateRepo,
    amountInputRepo: AmountInputRepo,
    mainThreadScheduler: Scheduler,
    defaultAmount: Amount,
    updatePeriodInSeconds: Float = 1f,
    coreLifecycleEvents: BehaviorSubject<Lifecycle.Event>
) : ActorReducerLifecycleFeature<Wish, Effect, State, News>(
    initialState = State(
        hasNetworkConnection = true,
        isLoading = true,
        rateSet = null,
        amounts = listOf(defaultAmount),
        selectedCurrency = defaultAmount.currency
    ),
    actor = ActorImpl(rateRepo, amountInputRepo, mainThreadScheduler),
    reducer = ReducerImpl(),
    newsPublisher = NewsPublisherImpl(),
    bootstrapper = BootstrapperImpl(
        updatePeriodInSeconds,
        Wish.ReloadRateSet,
        coreLifecycleEvents
    ),
    coreLifecycleEvents = coreLifecycleEvents
) {

    constructor(
        rateRepo: RateRepo,
        amountInputRepo: AmountInputRepo,
        mainThreadScheduler: Scheduler,
        defaultAmount: Amount,
        updatePeriodInSeconds: Float = 1f
    ) : this(
        rateRepo,
        amountInputRepo,
        mainThreadScheduler,
        amountInputRepo.getInput(defaultAmount),
        updatePeriodInSeconds,
        BehaviorSubject.create<Lifecycle.Event>()
    )


    data class State(
        val isLoading: Boolean = false,
        val rateSet: RateSet?,
        val amounts: List<Amount>,
        val selectedCurrency: Currency,
        val hasNetworkConnection: Boolean
    ) {
        val noConnection = !hasNetworkConnection
        val selectedCurrencyAmount
            get() = amounts.firstOrNull { it.currency == selectedCurrency }
    }

    sealed class Wish {
        object ReloadRateSet : Wish()
        data class SelectNewBaseCurrency(val selectedCurrency: Currency) : Wish()
        data class CalculateNewAmounts(val inputValue: BigDecimal) : Wish()
    }

    sealed class Effect {

        object RateSetLoadingStarted : Effect()

        data class RateSetLoadingFinished(
            val rateSet: RateSet,
            val isDataOffline: Boolean
        ) : Effect()

        data class RateSetLoadingError(val throwable: Throwable) : Effect()
        data class RecalculateAmount(val baseCurrencyValue: BigDecimal) : Effect()
        data class ChangeBaseCurrency(val selectedCurrency: Currency) : Effect()

        object HideSoftInput : Effect()
    }

    sealed class News {
        data class Error(val throwable: Throwable) : News()
        object HideSoftInput : News()
    }
}