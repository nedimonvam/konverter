package ru.int64.konverter.converter.ext

import ru.int64.rateset.Amount
import ru.int64.rateset.RateSet
import java.math.BigDecimal

fun RateSet.getAllCurrencies() = rates.map { it.dividend } + base

fun RateSet.calculateAllCurrencies(amount: Amount) = getAllCurrencies()
    .map { calculate(amount,it) }

fun RateSet.getCurrencyWithMaxCost() = when {
    rates.all { it.ratio > BigDecimal(1) } -> base
    else -> rates.minBy { it.ratio }?.dividend ?: base
}