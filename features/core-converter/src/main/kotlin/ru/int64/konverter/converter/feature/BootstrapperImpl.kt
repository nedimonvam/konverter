package ru.int64.konverter.converter.feature

import com.badoo.mvicore.binder.lifecycle.Lifecycle
import com.badoo.mvicore.element.Bootstrapper
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import java.util.concurrent.TimeUnit

/**
 * Bootstrapper that emits provided [wish] with provided [updatePeriod] in seconds
 */
class BootstrapperImpl<Wish>(
    private val updatePeriod: Float,
    private val wish: Wish,
    private val coreLifecycleEvents: Observable<Lifecycle.Event>
) : Bootstrapper<Wish> {

    private val interval =
        Observable.interval((updatePeriod * 1000).toLong(), TimeUnit.MILLISECONDS)

    override fun invoke(): Observable<Wish> = Observable
        .combineLatest(
            interval,
            coreLifecycleEvents,
            BiFunction<Long, Lifecycle.Event, Lifecycle.Event> { _, lifecycleEvent -> lifecycleEvent }
        )
        .filter { it == Lifecycle.Event.BEGIN }
        .map { wish }
}