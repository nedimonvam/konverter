package ru.int64.konverter.converter.feature

import com.badoo.mvicore.element.Actor
import io.reactivex.Observable
import io.reactivex.Observable.empty
import io.reactivex.Observable.just
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import ru.int64.konverter.converter.ext.getCurrencyWithMaxCost
import ru.int64.konverter.converter.feature.ConverterFeature.*
import ru.int64.konverter.converter.feature.ConverterFeature.Effect.*
import ru.int64.konverter.converter.feature.ConverterFeature.Wish.*
import ru.int64.konverter.interactor.common.RepoResult
import ru.int64.konverter.interactor.repos.AmountInputRepo
import ru.int64.konverter.interactor.repos.RateRepo
import ru.int64.rateset.ext.amountOf

class ActorImpl(
    private val rateRepo: RateRepo,
    private val amountInputRepo: AmountInputRepo,
    private val mainThreadScheduler: Scheduler
) : Actor<State, Wish, Effect> {

    override fun invoke(state: State, wish: Wish): Observable<out Effect> = when (wish) {

        /**
         * always load RateSet for the most expensive currency
         * to get more significant digits in rates
         * produce [Effect.RateSetLoadingError]
         */
        is ReloadRateSet -> rateRepo.getRateSet(
            state.rateSet?.getCurrencyWithMaxCost() ?: state.selectedCurrency
        ).toObservable()
            .map<Effect> {
                RateSetLoadingFinished(
                    it.result,
                    isDataOffline = it.dsType != RepoResult.DsType.API
                )
            }
            .onErrorReturn { RateSetLoadingError(it) }
            .observeOn(mainThreadScheduler)
            .subscribeOn(Schedulers.io())

        /**
         * produce [Effect.ChangeBaseCurrency] only if Currency was changed
         */
        is SelectNewBaseCurrency -> if (wish.selectedCurrency != state.selectedCurrency) {
            state.amounts.firstOrNull { it.currency == wish.selectedCurrency }?.let {
                amountInputRepo.setInput(it)
            }
            just(ChangeBaseCurrency(wish.selectedCurrency))
        } else {
            empty()
        }

        /**
         * produce [Effect.RecalculateAmount] only if input Amount was changed
         */
        is CalculateNewAmounts -> if (wish.inputValue != state.selectedCurrencyAmount?.value) {
            amountInputRepo.setInput(wish.inputValue amountOf state.selectedCurrency)
            just(RecalculateAmount(wish.inputValue))
        } else {
            empty()
        }
    }
}