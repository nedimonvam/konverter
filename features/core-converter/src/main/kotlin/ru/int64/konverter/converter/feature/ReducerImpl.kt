package ru.int64.konverter.converter.feature

import com.badoo.mvicore.element.Reducer
import ru.int64.konverter.converter.ext.calculateAllCurrencies
import ru.int64.konverter.converter.ext.getAllCurrencies
import ru.int64.konverter.converter.feature.ConverterFeature.Effect
import ru.int64.konverter.converter.feature.ConverterFeature.Effect.*
import ru.int64.konverter.interactor.repos.RateRepo
import ru.int64.rateset.ext.amountOf

class ReducerImpl : Reducer<ConverterFeature.State, Effect> {

    override fun invoke(
        state: ConverterFeature.State,
        effect: Effect
    ): ConverterFeature.State = when (effect) {
        is RateSetLoadingStarted -> {
            state.copy(
                isLoading = true
            )
        }
        is RateSetLoadingError -> if (effect.throwable is RateRepo.DataNotLoadedException)
            state.copy(
                hasNetworkConnection = false,
                isLoading = false
            )
        else {
            state.copy(
                isLoading = false
            )
        }
        is RateSetLoadingFinished -> {
            state.copy(
                hasNetworkConnection = !effect.isDataOffline,
                isLoading = false,
                rateSet = effect.rateSet,
                amounts = state.selectedCurrencyAmount?.let {
                    effect.rateSet.calculateAllCurrencies(it)
                } ?: state.amounts
            )
        }
        is ChangeBaseCurrency -> {
            state.copy(
                selectedCurrency = effect.selectedCurrency
            )
        }
        is RecalculateAmount -> {
            state.copy(
                amounts = state.rateSet?.getAllCurrencies()?.map {
                    if (it == state.selectedCurrency) {
                        effect.baseCurrencyValue amountOf it
                    } else {
                        state.rateSet.calculate(
                            inputAmount = effect.baseCurrencyValue amountOf state.selectedCurrency,
                            destinationCurrency = it
                        )
                    }
                } ?: listOf()
            )
        }
        is HideSoftInput -> state
    }
}