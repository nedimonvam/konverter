package ru.int64.konverter.converter.viewmodels

import ru.int64.rateset.Amount

open class CurrencyItem(
    val amount: Amount
)