package ru.int64.konverter.converter.feature

import org.assertj.core.api.Assertions.assertThat
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import ru.int64.rateset.RateSetBuilder
import ru.int64.rateset.ext.amountOf
import ru.int64.rateset.ext.currencyOf
import ru.int64.rateset.ext.rateTo
import java.math.BigDecimal

private val eur = currencyOf("EUR")
private val usd = currencyOf("USD")
private val rub = currencyOf("RUB")

private val rateSet1 = RateSetBuilder()
    .base(eur)
    .rates(listOf(eur.rateTo(usd, 1.142.toBigDecimal())))
    .build()

private val rateSet2 = RateSetBuilder()
    .base(eur)
    .rates(listOf(eur.rateTo(usd, 1.142.toBigDecimal()), eur.rateTo(rub, 75.26.toBigDecimal())))
    .build()

object ReducerSpek : Spek({

    describe("ConverterFeature - Reducer") {
        val reducer = ReducerImpl()

        context("Loaded state (isLoading == false; RateSet != null)") {
            val oldState = ConverterFeature.State(
                hasNetworkConnection = true,
                isLoading = false,
                rateSet = rateSet1,
                amounts = rateSet1.rates.map { it.calculate(1 amountOf eur) },
                selectedCurrency = eur
            )

            context("Effect: StartedLoading") {
                val effect = ConverterFeature.Effect.RateSetLoadingStarted

                reducer(oldState, effect).let { newState ->

                    it("should have isLoading == true") {
                        assertThat(newState.isLoading).isTrue()
                    }

                    it("should have the same baseCurrency") {
                        assertThat(newState.selectedCurrency).isEqualTo(oldState.selectedCurrency)
                    }

                    it("should have the same rateSet") {
                        assertThat(newState.rateSet).isEqualTo(oldState.rateSet)
                    }
                }
            }

            context("Effect: Loaded") {
                val effect = ConverterFeature.Effect.RateSetLoadingFinished(rateSet2, false)

                reducer(oldState, effect).let { newState ->

                    it("should have isLoading == false") {
                        assertThat(newState.isLoading).isFalse()
                    }

                    it("should have the same baseCurrency") {
                        assertThat(newState.selectedCurrency).isEqualTo(oldState.selectedCurrency)
                    }

                    it("should have the rateSet from Effect.Loaded") {
                        assertThat(newState.rateSet).isEqualTo(effect.rateSet)
                    }
                }
            }

            context("Effect: LoadingError") {
                val throwable = Exception("An exception")
                val effect = ConverterFeature.Effect.RateSetLoadingError(throwable)

                reducer(oldState, effect).let { newState ->

                    it("should have isLoading == false") {
                        assertThat(newState.isLoading).isFalse()
                    }

                    it("should have the same baseCurrency") {
                        assertThat(newState.selectedCurrency).isEqualTo(oldState.selectedCurrency)
                    }

                    it("should have previous RateSet") {
                        assertThat(newState.rateSet).isEqualTo(oldState.rateSet)
                    }
                }
            }

            context("Effect: CurrencySelectionChanged") {
                val newCurrency = currencyOf("rub")
                val effect = ConverterFeature.Effect.ChangeBaseCurrency(newCurrency)

                reducer(oldState, effect).let { newState ->

                    it("should have isLoading == false") {
                        assertThat(newState.isLoading).isFalse()
                    }

                    it("should have new baseCurrency") {
                        assertThat(newState.selectedCurrency).isEqualTo(newCurrency)
                    }

                    it("should have previous RateSet") {
                        assertThat(newState.rateSet).isEqualTo(oldState.rateSet)
                    }
                }
            }

            context("Effect: InputValueUpdated") {
                val newInputValue = BigDecimal(2)
                val effect = ConverterFeature.Effect.RecalculateAmount(newInputValue)

                reducer(oldState, effect).let { newState ->

                    it("should have isLoading == false") {
                        assertThat(newState.isLoading).isFalse()
                    }

                    it("should have the same baseCurrency") {
                        assertThat(newState.selectedCurrency).isEqualTo(oldState.selectedCurrency)
                    }

                    it("should have previous RateSet") {
                        assertThat(newState.rateSet).isEqualTo(oldState.rateSet)
                    }
                }
            }
        }
    }
})