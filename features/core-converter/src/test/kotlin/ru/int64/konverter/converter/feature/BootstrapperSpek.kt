package ru.int64.konverter.converter.feature

import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import com.badoo.mvicore.binder.lifecycle.Lifecycle
import java.util.concurrent.TimeUnit

object BootstrapperSpek : Spek({

    describe("ConverterFeature") {

        describe("Bootstrapper") {

            context("created for some updatePeriod and Wish") {
                data class TestWishType(val name: String)

                val updatePeriod = 1f // sec
                val testWish = TestWishType("test_wish_name")

                val bootstrapper by memoized {
                    BootstrapperImpl(
                        updatePeriod = updatePeriod,
                        wish = testWish,
                        coreLifecycleEvents = Observable.just<Lifecycle.Event>(Lifecycle.Event.BEGIN)
                    )
                }

                it("should emit provided Wish") {
                    val testObserver = TestObserver<TestWishType>()
                    bootstrapper().subscribe(testObserver)
                    testObserver.awaitCount(1)
                    testObserver.assertValue(testWish)
                }

                it("should emit every updatePeriod") {
                    val testObserver = TestObserver<TestWishType>()
                    bootstrapper().subscribe(testObserver)
                    testObserver.await(1500, TimeUnit.MILLISECONDS)
                    testObserver.assertValueCount(1)
                }
            }
        }
    }
})