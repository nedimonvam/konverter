package ru.int64.konverter.converter.ext

import org.assertj.core.api.Assertions.assertThat
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import ru.int64.rateset.RateSetBuilder
import ru.int64.rateset.ext.currencyOf
import ru.int64.rateset.ext.ratesTo

object RateSetExtensionsSpek : Spek({

    describe("getAllCurrencies() extension") {

        context("for RateSet with N Rates") {
            val base = currencyOf("base")
            val otherCurrencies = (1..10).map { currencyOf("code$it") }
            val rateSet by memoized {
                RateSetBuilder()
                    .base(base)
                    .rates(base.ratesTo(*otherCurrencies.map { it to 1.toBigDecimal() }.toTypedArray()))
                    .build()
            }

            describe("should return List") {

                it("of N+1 Currencies") {
                    assertThat(rateSet.getAllCurrencies()).hasSize(otherCurrencies.size + 1)
                }

                it("containing all dividend Currencies of Rates in RateSet") {
                    assertThat(rateSet.getAllCurrencies()).containsAll(otherCurrencies)
                }

                it("containing base Currency") {
                    assertThat(rateSet.getAllCurrencies()).contains(base)
                }
            }
        }
    }
})