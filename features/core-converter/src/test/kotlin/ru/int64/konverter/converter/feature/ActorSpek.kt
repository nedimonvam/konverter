package ru.int64.konverter.converter.feature

import io.mockk.every
import io.mockk.mockk
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import org.assertj.core.api.Assertions.assertThat
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import ru.int64.konverter.interactor.repos.AmountInputRepo
import ru.int64.lib.appresult.AppResult
import ru.int64.lib.network.networkstate.NetworkConnectionDs
import ru.int64.rateset.RateSetBuilder
import ru.int64.rateset.ds.RateApiDs
import ru.int64.rateset.ds.RatePersistenceDs
import ru.int64.rateset.ext.amountOf
import ru.int64.rateset.ext.currencyOf
import ru.int64.rateset.ext.rateTo
import ru.int64.rateset.repo.RateRepoImpl
import java.math.BigDecimal

private val eur = currencyOf("EUR")
private val usd = currencyOf("USD")
private val rub = currencyOf("RUB")

private val rateSet = RateSetBuilder()
    .base(eur)
    .rates(listOf(eur.rateTo(usd, 1.142.toBigDecimal())))
    .build()

private val newRateSet = RateSetBuilder()
    .base(eur)
    .rates(listOf(eur.rateTo(usd, 1.142.toBigDecimal()), eur.rateTo(rub, 75.26.toBigDecimal())))
    .build()

object ActorSpek : Spek({

    val rateSetRepo = RateRepoImpl(
        mockk<RateApiDs>().apply {
            every { getRateSet(eur) } returns AppResult.Success(newRateSet)
        },
        mockk<RatePersistenceDs>().apply {
            every { saveRateSet(newRateSet) } returns Unit
        },
        mockk<NetworkConnectionDs>().apply {
            every { getIsConnected() } returns true
        }
    )

    val amountInputRepo = mockk<AmountInputRepo>().apply {
        every { setInput(any()) } returns Unit
        every { getInput(any()) } returns (1 amountOf "eur")
    }

    describe("ConverterFeature - Actor") {
        val actor = ActorImpl(rateSetRepo, amountInputRepo, Schedulers.trampoline())

        context("common case State") {

            // Feature settings
            val defaultCurrency = eur

            // Initial state example for reusing
            val initialState = ConverterFeature.State(
                hasNetworkConnection = true,
                isLoading = true,
                rateSet = rateSet,
                amounts = rateSet.rates.map { it.calculate(1 amountOf eur) },
                selectedCurrency = defaultCurrency
            )

            context("Wish: ReloadRateSet") {
                val wish = ConverterFeature.Wish.ReloadRateSet

                describe("Effect") {
                    actor(initialState, wish).next().let { effect ->

                        it("should be Effect.Loaded") {
                            assertThat(effect).isInstanceOf(ConverterFeature.Effect.RateSetLoadingFinished::class.java)
                        }
                        (effect as? ConverterFeature.Effect.RateSetLoadingFinished)?.let { loaded ->
                            it("should contain the RateSet from ds") {
                                assertThat(loaded.rateSet).isEqualTo(newRateSet)
                            }
                        }
                    }
                }
            }

            context("Wish: SelectNewBaseCurrency - new currency") {
                val newBaseCurrency = currencyOf("rub")
                val wish = ConverterFeature.Wish.SelectNewBaseCurrency(newBaseCurrency)

                describe("Effect") {
                    actor(initialState, wish).next().let { effect ->

                        it("should be Effect.CurrencySelectionChanged") {
                            assertThat(effect).isInstanceOf(ConverterFeature.Effect.ChangeBaseCurrency::class.java)
                        }
                        effect as ConverterFeature.Effect.ChangeBaseCurrency

                        it("should have new selectedCurrency") {
                            assertThat(effect.selectedCurrency).isEqualTo(newBaseCurrency)
                        }
                    }
                }
            }

            context("Wish: SelectNewBaseCurrency - the same currency") {
                val wish = ConverterFeature.Wish.SelectNewBaseCurrency(defaultCurrency)

                it("Effect should be null") {
                    actor(initialState, wish).test().assertNoValues()
                }
            }

            context("Wish: CalculateNewAmounts - new value") {
                val newValue = BigDecimal(2)
                val wish = ConverterFeature.Wish.CalculateNewAmounts(newValue)

                describe("Effect") {
                    actor(initialState, wish).next().let { effect ->

                        it("should be Effect.InputValueUpdated") {
                            assertThat(effect).isInstanceOf(ConverterFeature.Effect.RecalculateAmount::class.java)
                        }
                        effect as ConverterFeature.Effect.RecalculateAmount

                        it("should have new selectedCurrency") {
                            assertThat(effect.baseCurrencyValue).isEqualTo(newValue)
                        }
                    }
                }
            }
        }

    }
})

private fun <T> Observable<T>.next(): T = test()
    .awaitCount(1)
    .values()
    .first()
