package ru.int64.konverter.converter.feature

import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import ru.int64.konverter.converter.feature.ConverterFeature.Effect
import ru.int64.rateset.RateSetBuilder
import ru.int64.rateset.ext.amountOf
import ru.int64.rateset.ext.currencyOf
import ru.int64.rateset.ext.rateTo

private val eur = currencyOf("EUR")
private val usd = currencyOf("USD")

private val rateSet = RateSetBuilder()
    .base(eur)
    .rates(listOf(eur.rateTo(usd, 1.142.toBigDecimal())))
    .build()

object NewsPublisherSpek : Spek({
    // Initial state example for reusing
    val initialState = ConverterFeature.State(
        hasNetworkConnection = true,
        isLoading = true,
        rateSet = rateSet,
        amounts = rateSet.rates.map { it.calculate(1 amountOf eur) },
        selectedCurrency = eur
    )

    describe("ConverterFeature - NewsPublisher") {
        val publisher = NewsPublisherImpl()

        context("Effect Effect.LoadingError") {
            val exception = Exception("error message")
            val wish = ConverterFeature.Wish.ReloadRateSet
            val effect = Effect.RateSetLoadingError(exception)
            val expected = ConverterFeature.News.Error(exception)

            it("should produce News.Error") {
                assertThat(publisher(
                    wish = wish,
                    effect = effect,
                    state = initialState
                )).isEqualTo(expected)
            }
        }
        listOf(
            Effect.RateSetLoadingStarted,
            Effect.RateSetLoadingFinished(rateSet, true),
            Effect.RateSetLoadingFinished(rateSet, false),
            Effect.ChangeBaseCurrency(eur),
            Effect.RecalculateAmount(1.2.toBigDecimal())
        ).forEach { effect ->

            context("Effect ${effect.javaClass.simpleName}") {
                it("should never lead to any News producing") {
                    assertThat(
                        // wish and state are ignored in NewsPublisherImpl
                        publisher.invoke(
                            wish = mockk(),
                            effect = effect,
                            state = mockk()
                        )
                    ).isEqualTo(null)
                }
            }
        }
    }
})