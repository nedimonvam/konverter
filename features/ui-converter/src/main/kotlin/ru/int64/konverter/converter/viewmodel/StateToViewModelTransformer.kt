package ru.int64.konverter.converter.viewmodel

import ru.int64.konverter.converter.feature.ConverterFeature
import ru.int64.konverter.converter.viewmodels.ConverterInputCurrencyItem

class StateToViewModelTransformer : (ConverterFeature.State) -> ViewModel {

    override fun invoke(state: ConverterFeature.State): ViewModel = run {
        when (state.rateSet) {
            null -> state.run {
                ViewModel(
                    currencyItems = listOf(),
                    selectedRateItem = null,
                    loadingVisible = isLoading,
                    networkIndicatorVisible = noConnection,
                    networkOverlayVisible = noConnection && !isLoading // don't show while loading
                )
            }
            else -> {
                // find selected currency in amounts or take any if not found
                val selectedCurrencyAmount = state.amounts
                    .firstOrNull { it.currency == state.selectedCurrency }
                    ?: state.amounts.first()

                val selectedItem = ConverterInputCurrencyItem(
                    amount = selectedCurrencyAmount,
                    isActive = true
                )


                val otherItems = state.amounts
                    .minus(selectedItem.amount) // exclude selected currency item
                    .map { amount ->
                        ConverterInputCurrencyItem(
                            amount,
                            isActive = false
                        )
                    } // create the rest of items
                    .sortedBy { it.amount.currency.code }

                // concat items - selected first
                ViewModel(
                    currencyItems = listOf(selectedItem) + otherItems,
                    selectedRateItem = selectedItem,
                    loadingVisible = state.isLoading,
                    networkIndicatorVisible = !state.hasNetworkConnection,
                    networkOverlayVisible = false
                )
            }
        }
    }
}