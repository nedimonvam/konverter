package ru.int64.konverter.converter.uievents

import ru.int64.rateset.Currency

sealed class UiEvent {
    data class CurrencyListItemClick(val selectedCurrency: Currency) : UiEvent()
    data class SelectedListItemInputChanged(val inputValue: String) : UiEvent()
}