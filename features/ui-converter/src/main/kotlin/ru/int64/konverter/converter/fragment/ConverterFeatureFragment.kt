package ru.int64.konverter.converter.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.badoo.mvicore.binder.lifecycle.Lifecycle
import com.badoo.mvicore.modelWatcher
import io.reactivex.functions.Consumer
import io.reactivex.subjects.BehaviorSubject
import org.koin.android.ext.android.inject
import org.koin.core.qualifier.named
import ru.int64.konverter.converter.feature.ConverterFeature
import ru.int64.konverter.converter.viewmodel.ViewModel
import ru.int64.konverter.shared.ratefeatures.ui.databinding.FragmentRateListBinding
import ru.int64.konverter.shared.ratefeatures.ui.ext.scrollLifecycleEvents
import ru.int64.konverter.shared.ratefeatures.ui.ext.showIf
import ru.int64.konverter.shared.ratefeatures.ui.mvi.CommonNewsConsumer
import ru.int64.konverter.shared.ratefeatures.ui.utils.SoftInputUtil
import ru.int64.lib.mvilifecycle.android.viewpager.ViewPagerFragmentLifecycle

class ConverterFeatureFragment : Fragment(),
    ViewPagerFragmentLifecycle {

    override val viewPagerLifecycleEvents = BehaviorSubject.createDefault(Lifecycle.Event.BEGIN)

    private val feature: ConverterFeature by inject(named<ConverterFeatureFragment>())

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentRateListBinding
        .inflate(inflater, container, false)
        .also { setupFragment(it) }
        .root

    private fun setupFragment(viewBinding: FragmentRateListBinding) {
        // setup UI
        val recyclerAdapter = ConverterRateListRecyclerAdapter()
        viewBinding.recycler.apply {
            adapter = recyclerAdapter
            layoutManager = LinearLayoutManager(requireContext())
            itemAnimator?.changeDuration = 0 // to avoid blinking on update
        }
        // setup MVI bindings
        MviBindings(
            this,
            feature,
            viewModelConsumer = Consumer(modelWatcher<ViewModel> {
                viewBinding.apply {
                    watch(ViewModel::networkIndicatorVisible) {
                        txtNetworkProgressBar.showIf(it)
                        txtNetworkIndicator.showIf(it)
                    }
                    watch(ViewModel::networkOverlayVisible) { txtNetworkOverlay.showIf(it) }
                    watch(ViewModel::loadingVisible) { progressBar.showIf(it) }
                    watch(ViewModel::currencyItems) { recyclerAdapter.submitList(it) }
                }
            }::invoke),
            newsActionConsumer = CommonNewsConsumer(this),
            uiEventSource = recyclerAdapter,
            additionalLifecycleEventSources = listOf(
                viewBinding.recycler.scrollLifecycleEvents(),
                viewPagerLifecycleEvents
            )
        )
    }

    override fun onPause() {
        super.onPause()
        // hide keyboard when switched to another ViewPager page
        SoftInputUtil.hideSoftKeyboard(this)
    }
}
