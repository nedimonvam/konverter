package ru.int64.konverter.converter.fragment

import android.content.Context
import io.reactivex.Observer
import ru.int64.konverter.converter.fragment.utils.AfterTextChangedTextWatcher
import ru.int64.konverter.converter.fragment.utils.DecimalInputFilter
import ru.int64.konverter.converter.ui.R
import ru.int64.konverter.converter.ui.databinding.ItemRateInputBinding
import ru.int64.konverter.converter.uievents.UiEvent
import ru.int64.konverter.converter.uievents.UiEvent.CurrencyListItemClick
import ru.int64.konverter.converter.uievents.UiEvent.SelectedListItemInputChanged
import ru.int64.konverter.converter.viewmodels.ConverterInputCurrencyItem
import ru.int64.konverter.shared.flags.getNameResIdOrNull
import ru.int64.konverter.shared.flags.getRoundIconResIdOrNull
import ru.int64.konverter.shared.ratefeatures.ui.utils.SoftInputUtil

object InputRateItemViewBinder {

    fun bind(
        item: ConverterInputCurrencyItem,
        binding: ItemRateInputBinding,
        context: Context,
        uiEvents: Observer<UiEvent>
    ) {
        binding.apply {

            // bind simple fields
            txtCurrencyCode.text = item.amount.currency.code
            editTxt.setText(context.getString(R.string.format_float_amount, item.amount.value))

            // bind ui events
            editTxt.apply {
                // item selection
                setOnFocusChangeListener { _, isFocused ->
                    if (isFocused) {
                        uiEvents.onNext(CurrencyListItemClick(item.currency))
                    }
                }
                // input changed
                addTextChangedListener(AfterTextChangedTextWatcher {
                    if (hasFocus()) {
                        uiEvents.onNext(SelectedListItemInputChanged(it.replace(",", "")))
                    }
                })
                filters = arrayOf(DecimalInputFilter(2))
            }

            // Hide keyboard when item lost focus
            if (txtRateValue.hasFocus()) {
                txtRateValue.setOnFocusChangeListener { _, isFocused ->
                    if (!isFocused) SoftInputUtil.hideSoftKeyboard(txtRateValue)
                }
            }

            // bind resources from :res-flags
            item.amount.currency.apply {
                getNameResIdOrNull()?.let { txtCurrencyName.text = context.getString(it) }
                getRoundIconResIdOrNull()?.let { imgCountryFlag.setImageResource(it) }
            }
        }
    }
}