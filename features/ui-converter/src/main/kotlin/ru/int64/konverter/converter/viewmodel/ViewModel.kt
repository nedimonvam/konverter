package ru.int64.konverter.converter.viewmodel

import ru.int64.konverter.converter.viewmodels.ConverterInputCurrencyItem
import ru.int64.konverter.shared.ratefeatures.ui.mvi.RateListViewModel

data class ViewModel(
    val currencyItems: List<ConverterInputCurrencyItem>,
    val selectedRateItem: ConverterInputCurrencyItem?,
    override val loadingVisible: Boolean,
    override val networkIndicatorVisible: Boolean,
    override val networkOverlayVisible: Boolean = false
) : RateListViewModel()