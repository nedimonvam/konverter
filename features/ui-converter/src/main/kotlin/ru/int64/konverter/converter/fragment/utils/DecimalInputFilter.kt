package ru.int64.konverter.converter.fragment.utils

import android.text.InputFilter
import android.text.Spanned

class DecimalInputFilter(private val maxDecimalDigits: Int) : InputFilter {
    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned,
        dstart: Int,
        dend: Int
    ): CharSequence? {
        val dividerIndex = dest.indexOfFirst { !it.isDigit() }
        when {
            dividerIndex >= 0 -> {
                when {
                    dend - dstart == dest.length -> return null
                    // do not limit input before divider
                    dend <= dividerIndex -> return null
                    // limit symbols after divider
                    dest.length - dividerIndex - dend + dstart > maxDecimalDigits -> return ""
                }
            }
            source.isDivider() && (dest.length - dend > maxDecimalDigits) -> return ""
        }
        return null
    }

    companion object {
        fun CharSequence.isDivider() = equals(".") || equals(",")
    }
}