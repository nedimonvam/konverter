package ru.int64.konverter.converter.fragment

import com.badoo.mvicore.binder.lifecycle.Lifecycle
import com.badoo.mvicore.binder.using
import io.reactivex.ObservableSource
import io.reactivex.functions.Consumer
import ru.int64.konverter.converter.feature.ConverterFeature
import ru.int64.konverter.converter.uievents.UiEvent
import ru.int64.konverter.converter.uievents.UiEventToWishTransformer
import ru.int64.konverter.converter.viewmodel.StateToViewModelTransformer
import ru.int64.konverter.converter.viewmodel.ViewModel
import ru.int64.konverter.shared.ratefeatures.ui.mvi.news.ErrorLogger
import ru.int64.konverter.shared.ratefeatures.ui.mvi.news.NewsAction
import ru.int64.konverter.shared.ratefeatures.ui.mvi.news.NewsToNewsActionTransformer
import ru.int64.lib.lib.mvi.lifecycle.ext.foldLifecycleSources
import ru.int64.lib.mvilifecycle.android.bindings.BinderBehavior
import ru.int64.lib.mvilifecycle.android.bindings.GenericAndroidLifecycleFeatureBindings
import ru.int64.lib.mvilifecycle.android.ext.getLifecycleEventSource

internal class MviBindings(
    view: ConverterFeatureFragment,
    private val feature: ConverterFeature,
    uiEventSource: ObservableSource<UiEvent>,
    viewModelConsumer: Consumer<ViewModel>,
    newsActionConsumer: Consumer<NewsAction>,
    additionalLifecycleEventSources: List<ObservableSource<Lifecycle.Event>> = listOf()
) : GenericAndroidLifecycleFeatureBindings<UiEvent, ViewModel, NewsAction>(
    feature,
    viewLifecycleEvents = foldLifecycleSources(
        additionalLifecycleEventSources + view.viewLifecycleOwner.getLifecycleEventSource(
            BinderBehavior.ResumePause
        )
    ),
    coreLifecycleEvents = foldLifecycleSources(
        listOf(view.viewLifecycleOwner.getLifecycleEventSource(BinderBehavior.ResumePause))
    ),
    setup = { viewBinder ->
        viewBinder.run {
            // view -> feature
            bind(uiEventSource to feature using UiEventToWishTransformer())
            // feature -> view
            bind(feature to viewModelConsumer using StateToViewModelTransformer())
            bind(feature.news to newsActionConsumer using NewsToNewsActionTransformer())
            bind(feature.news to ErrorLogger("Converter") using { it as? ConverterFeature.News.Error })
        }
    }
)