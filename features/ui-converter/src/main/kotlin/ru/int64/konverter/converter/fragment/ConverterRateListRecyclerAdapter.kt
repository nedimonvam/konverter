package ru.int64.konverter.converter.fragment

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.viewbinding.ViewBinding
import io.reactivex.ObservableSource
import io.reactivex.Observer
import io.reactivex.subjects.PublishSubject
import ru.int64.konverter.shared.ratefeatures.ui.utils.SoftInputUtil
import ru.int64.konverter.converter.ui.databinding.ItemRateInputBinding
import ru.int64.konverter.converter.uievents.UiEvent
import ru.int64.konverter.converter.viewmodels.ConverterInputCurrencyItem
import ru.int64.konverter.shared.ratefeatures.ui.adapters.BaseRecyclerViewAdapter

class ConverterRateListRecyclerAdapter :
    ObservableSource<UiEvent>,
    BaseRecyclerViewAdapter<ConverterInputCurrencyItem>(diffCallback) {

    private val uiEvents = PublishSubject.create<UiEvent>()

    override fun subscribe(observer: Observer<in UiEvent>) = uiEvents.subscribe(observer)

    override fun getItemId(position: Int) = getItem(position).amount.currency.hashCode().toLong()

    override fun inflateBinding(
        layoutInflater: LayoutInflater,
        viewType: Int,
        parent: ViewGroup,
        attachToRoot: Boolean
    ): ViewBinding = ItemRateInputBinding.inflate(layoutInflater, parent, false)

    override fun bind(
        item: ConverterInputCurrencyItem,
        binding: ViewBinding,
        context: Context,
        itemViewType: Int
    ) = InputRateItemViewBinder.bind(item, binding as ItemRateInputBinding, context, uiEvents)

    /**
     * Hide keyboard when User scrolls down
     */
    override fun onViewDetachedFromWindow(holder: ViewHolder<ConverterInputCurrencyItem>) {
        super.onViewDetachedFromWindow(holder)
        if ((holder.binding as ItemRateInputBinding).editTxt.hasFocus()) {
            SoftInputUtil.hideSoftKeyboard(holder.itemView)
        }
    }

    companion object {

        /**
         * Simple DiffUtil.ItemCallback that
         * 1. Supposes [ConverterInputCurrencyItem.amount] as content
         * 2. Supposes its Amount.currency field as ID
         * 3. Supposes that active (focused) [ConverterInputCurrencyItem] NEVER requires update
         */
        val diffCallback = object : DiffUtil.ItemCallback<ConverterInputCurrencyItem>() {
            override fun areItemsTheSame(
                oldItem: ConverterInputCurrencyItem,
                newItem: ConverterInputCurrencyItem
            ) = oldItem.amount.currency == newItem.amount.currency

            override fun areContentsTheSame(
                oldItem: ConverterInputCurrencyItem,
                newItem: ConverterInputCurrencyItem
            ) = oldItem.amount == newItem.amount ||
                // never update content for the active/focused item (current input)
                (areItemsTheSame(oldItem, newItem)
                    && oldItem.isActive && newItem.isActive
                    || oldItem.amount == newItem.amount)
        }
    }
}