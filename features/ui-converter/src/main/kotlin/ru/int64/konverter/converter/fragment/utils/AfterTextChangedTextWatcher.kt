package ru.int64.konverter.converter.fragment.utils

import android.text.Editable
import android.text.TextWatcher

class AfterTextChangedTextWatcher(
    private val afterTextChangedListener: (String) -> Unit
) : TextWatcher {

    override fun afterTextChanged(p0: Editable?) = afterTextChangedListener(p0.toString())

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit
    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit
}