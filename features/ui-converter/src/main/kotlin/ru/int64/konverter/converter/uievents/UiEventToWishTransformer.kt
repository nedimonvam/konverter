package ru.int64.konverter.converter.uievents

import ru.int64.konverter.converter.feature.ConverterFeature
import ru.int64.konverter.converter.feature.ConverterFeature.Wish.CalculateNewAmounts
import ru.int64.konverter.converter.feature.ConverterFeature.Wish.SelectNewBaseCurrency

class UiEventToWishTransformer : (UiEvent) -> ConverterFeature.Wish? {

    override fun invoke(event: UiEvent): ConverterFeature.Wish? = when (event) {
        is UiEvent.CurrencyListItemClick -> SelectNewBaseCurrency(event.selectedCurrency)
        is UiEvent.SelectedListItemInputChanged -> CalculateNewAmounts(
            event.inputValue.toBigDecimalOrNull() ?: 0.toBigDecimal()
        )
    }
}