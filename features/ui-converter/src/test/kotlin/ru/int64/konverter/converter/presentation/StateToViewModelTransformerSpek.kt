package ru.int64.konverter.converter.presentation

import org.assertj.core.api.Assertions.assertThat
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import ru.int64.konverter.converter.ext.getAllCurrencies
import ru.int64.konverter.converter.feature.ConverterFeature
import ru.int64.konverter.converter.viewmodel.StateToViewModelTransformer
import ru.int64.rateset.Amount
import ru.int64.rateset.RateSet
import ru.int64.rateset.RateSetBuilder
import ru.int64.rateset.ext.amountOf
import ru.int64.rateset.ext.currencyOf
import ru.int64.rateset.ext.rateTo

private val eur = currencyOf("EUR")
private val usd = currencyOf("USD")
private val rub = currencyOf("RUB")

private val rateEurUsd = eur.rateTo(usd, 1.142.toBigDecimal())
private val rateEurRub = eur.rateTo(rub, 75.26.toBigDecimal())

private val rateSet = RateSetBuilder()
    .base(eur)
    .rates(listOf(rateEurUsd, rateEurRub))
    .build()

/**
 * Get List of [Amount] in all Currencies in [RateSet] including the [input] Amount
 */
fun calculateAmounts(rateSet: RateSet, input: Amount) = rateSet.rates
    .map { rate -> rate.calculate(input) }
    .plus(input)

object StateToViewModelTransformerSpek : Spek({

    describe("Converter Feature State -> ViewModel Transformation") {
        val transformer = StateToViewModelTransformer()

        context("Initial state: data loading in progress") {

            context(
                """[ConverterFeature.State]:
                            isLoading = true;
                            rateSet = null;
                            no amounts;
                            input = 1 EUR"""
            ) {
                val state = ConverterFeature.State(
                    hasNetworkConnection = true,
                    isLoading = true,
                    rateSet = null,
                    amounts = listOf(),
                    selectedCurrency = currencyOf("eur")
                )
                val viewModel = transformer(state)

                describe("viewModel") {
                    viewModel.apply {

                        it("isLoading = true") {
                            assertThat(loadingVisible).isTrue()
                        }

                        it("currencyItems is empty") {
                            assertThat(currencyItems).isEmpty()
                        }

                        it("selectedRateItem is null") {
                            assertThat(selectedRateItem).isNull()
                        }
                    }
                }
            }
        }

        context("Data was loaded from server") {

            context("Input: 1 EUR; RateSet contains EUR") {

                context(
                    """[ConverterFeature.State]: 
                            isLoading = false;
                            rateSet: EUR|USD|RUB;
                            3 amounts;
                            input: 1 EUR;"""
                ) {
                    val state = ConverterFeature.State(
                        hasNetworkConnection = true,
                        isLoading = false,
                        rateSet = rateSet,
                        amounts = calculateAmounts(rateSet, 1 amountOf eur),
                        selectedCurrency = eur
                    )
                    val viewModel = transformer(state)

                    describe("viewModel") {
                        viewModel.apply {

                            it("should have isLoading = false") {
                                assertThat(loadingVisible).isFalse()
                            }

                            it("currencyItems should have 3 items") {
                                assertThat(currencyItems).hasSize(3)
                            }

                            it("selectedRateItem should be not null") {
                                assertThat(selectedRateItem).isNotNull()
                            }

                            it("selectedRateItem currency should be EUR") {
                                assertThat(selectedRateItem?.currency).isEqualTo(eur)
                            }

                            it("currencyItems size should be equal state amounts size") {
                                assertThat(currencyItems).hasSameSizeAs(state.amounts)
                            }

                            it("currencyItems amounts should be equal amounts from state") {
                                val expected = calculateAmounts(rateSet, 1 amountOf eur)
                                assertThat(currencyItems.map { it.amount })
                                    .containsExactlyInAnyOrderElementsOf(expected)
                            }
                        }
                    }
                }
            }

            context("Input: 1 AUD; RateSet DOES NOT contain AUD") {
                // this case may happen when selected value is default
                // and it didn't appear in the server response

                context(
                    """[ConverterFeature.State]: 
                            isLoading = false;
                            rateSet: EUR|USD|RUB;
                            3 amounts;
                            input: 1 AUD;"""
                ) {
                    val state = ConverterFeature.State(
                        hasNetworkConnection = true,
                        isLoading = false,
                        rateSet = rateSet,
                        amounts = calculateAmounts(rateSet, 1 amountOf eur),
                        selectedCurrency = currencyOf("AUD")
                    )
                    val viewModel = transformer(state)

                    describe("viewModel") {
                        viewModel.apply {

                            it("should have isLoading = false") {
                                assertThat(loadingVisible).isFalse()
                            }

                            it("currencyItems should have 3 items") {
                                assertThat(currencyItems).hasSize(3)
                            }

                            it("currencyItems size should be equal state amounts size") {
                                assertThat(currencyItems).hasSameSizeAs(state.amounts)
                            }

                            it("selectedRateItem should be not null") {
                                assertThat(selectedRateItem).isNotNull()
                            }

                            it("selectedRateItem currency should be any from RateSet") {
                                assertThat(selectedRateItem?.currency).isIn(rateSet.getAllCurrencies())
                            }
                        }
                    }
                }
            }
        }
    }
})