package ru.int64.persistinput.repo

import ru.int64.konverter.interactor.repos.AmountInputRepo
import ru.int64.rateset.Amount

class AmountInputRepoImpl(
    private val amountInputDs: AmountInputDs
) : AmountInputRepo {

    override fun getInput(defaultAmount: Amount) = amountInputDs.getInput(defaultAmount)

    override fun setInput(input: Amount) = amountInputDs.setInput(input)
}