package ru.int64.persistinput.repo

import ru.int64.rateset.Amount

interface AmountInputDs {
    fun setInput(input: Amount)

    fun getInput(defaultAmount: Amount): Amount
}