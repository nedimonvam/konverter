package ru.int64.pesistinput.ds

import android.content.Context
import android.content.Context.MODE_PRIVATE
import ru.int64.persistinput.repo.AmountInputDs
import ru.int64.rateset.Amount
import ru.int64.rateset.ext.amountOf
import ru.int64.rateset.ext.currencyOf
import java.math.BigDecimal
import java.text.DecimalFormat

class AmountInputDsPrefsImpl(
    context: Context,
    sharedPrefsName: String
) : AmountInputDs {

    private val sharedPreferences = context.getSharedPreferences(NAME_PREFIX + sharedPrefsName, MODE_PRIVATE)

    private val format = DecimalFormat().apply { isParseBigDecimal = true }

    override fun setInput(input: Amount) {
        sharedPreferences.edit()
            .putString(KEY_INPUT_CURRENCY, input.currency.code)
            .putString(KEY_INPUT_VALUE, format.format(input.value))
            .apply()
    }

    override fun getInput(defaultAmount: Amount): Amount {
        val currencyCode = sharedPreferences.getString(
            KEY_INPUT_CURRENCY,
            null
        )
        val amountValueString = sharedPreferences.getString(
            KEY_INPUT_VALUE,
            null
        )
        return if (currencyCode == null || amountValueString == null) {
            defaultAmount
        } else {
            (format.parse(amountValueString) as BigDecimal) amountOf currencyOf(currencyCode)
        }
    }

    companion object {
        const val NAME_PREFIX = "input_persistence_"
        const val KEY_INPUT_VALUE = "input_value"
        const val KEY_INPUT_CURRENCY = "input_currency"
    }
}