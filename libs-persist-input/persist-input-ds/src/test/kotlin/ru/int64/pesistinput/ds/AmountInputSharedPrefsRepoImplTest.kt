package ru.int64.pesistinput.ds

import com.github.ivanshafran.sharedpreferencesmock.SPMockBuilder
import org.assertj.core.api.Assertions
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import ru.int64.rateset.ext.amountOf

object AmountInputRepoImplSpek : Spek({
    describe("AmountInputSharedPrefsRepoImpl") {

        val defaultAmount = 1 amountOf "eur"

        val repo by memoized {
            AmountInputDsPrefsImpl(
                SPMockBuilder().createContext(),
                "some feature name"
            )
        }

        context("if no value was set") {

            it("should return default value on first call") {
                Assertions.assertThat(repo.getInput(defaultAmount)).isEqualTo(defaultAmount)
            }
        }

        context("if new value was set") {
            val input = 2 amountOf "rub"

            it("returns the same value") {
                repo.setInput(input)
                Assertions.assertThat(repo.getInput(defaultAmount)).isEqualTo(input)
            }
        }
    }
})